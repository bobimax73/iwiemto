﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;


namespace IWiemTo.Data
{
    public class AppDBContext : DbContext
    {
        public AppDBContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Models.Questions> Questions { get; set; }
        public DbSet<Models.QuestionsChoice> QuestionChoice { get; set; }
        public DbSet<Models.Answers> Answers { get; set; }
        public DbSet<Models.QuestionsImage> QuestionsImage { get; set; }
        

        public DbSet<Models.QuestionCategory> QuestionCategory { get; set; }
        public DbSet<Models.Dict_QuestionsType> Dict_QuestionsType { get; set; }
        public DbSet<Models.Dict_Category> Dict_Category { get; set;}

        public DbSet<Models.Dict_SubCategory>Dict_SubCategory { get; set; }

        public DbSet<Models.LevelKnowledgeTable> LevelKnowledgeTable { get; set; }

        public DbSet<Models.QuestionsView> QuestionsView { get; set; }

        public DbSet<Models.Users> Users { get; set; }

        public DbSet<Models.Quiz>  Quiz { get; set; }

        public DbSet<Models.Dict_QuizMode> Dict_QuizMode { get; set; }

        public DbSet<Models.QuizQuestionsDetail> QuizQuestionsDetail { get; set; }

        public DbSet<Models.QuestionsBlocking> QuestionsBlocking { get; set; }

        public DbSet<Models.Dict_CategoryQuestionView> Dict_CategoryQuestionView { get; set; }

        public DbSet<Models.Dict_ResultsRange> Dict_ResultsRange { get; set; }


        public DbSet<Models.V_DashboardQuizTopResults> V_DashboardQuizTopResults { get; set; }

        public DbSet<Models.V_DashboardQuizSummaryInfo> V_DashboardQuizSummaryInfo { get; set; }

        public DbSet<Models.V_DashboardQuizTopQuestions> V_DashboardQuizTopQuestions { get; set; }

        public DbSet<Models.V_DashboardQuizQuestions> V_DashboardQuizQuestions { get; set; }

        public DbSet<Models.V_DashboardQuizProcentResults> V_DashboardQuizProcentResults { get; set; }

        public DbSet<Models.V_DashboardQuizTopSubCategory> V_DashboardQuizTopSubCategory { get; set; }

        public DbSet<Models.V_DashboardQuizActivites> V_DashboardQuizActivites { get; set; }

        public DbSet<Models.Dict_CommentsType> Dict_CommentsType { get; set; }

        //13.09.2019
        // mapowanie tabeli QuizQuestionsDetailComments
        public DbSet<Models.QuizQuestionsDetailComments> QuizQuestionsDetailComments { get; set; }

        // dodane 21.11.2019
        // view V_DashboardUsersComments

        public DbSet<Models.V_DashboardUsersComments> V_DashboardUsersComments { get; set; }

        //dreklaracja tabeli QuizQuestionsDetailComments dodane 30.11.2019
        public DbSet<Models.TTickets> Tickets { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            
            modelBuilder.Entity<Models.Quiz>().HasKey(c => c.QuizID);
            modelBuilder.Entity<Models.QuizQuestionsDetailComments>().HasKey(c => c.QuizQuestionsDetailCommentsID);
            modelBuilder.Entity<Models.QuizQuestionsDetailComments>().HasIndex(c => c.QuizQuestionDetailID);
            modelBuilder.Entity<Models.QuizQuestionsDetailComments>().HasIndex(c => c.CommentsTypeID);

            modelBuilder.Entity<Models.Questions>().HasKey(c => c.QuestionID);

            modelBuilder.Entity<Models.QuestionsImage>().HasKey(c => c.ImageID);


          


            modelBuilder.Entity<Models.QuestionCategory>().HasKey(c => c.QuestionCategoryID);
            modelBuilder.Entity<Models.QuestionsChoice>().HasKey(c => c.QuestionChoiceID);
            modelBuilder.Entity<Models.QuestionsChoice>().HasIndex(c => c.QuestionID);
            modelBuilder.Entity<Models.Dict_QuestionsType>().HasKey(c => c.TypeID);
            modelBuilder.Entity<Models.Dict_CommentsType>().HasKey(c => c.CommentsTypeID);

            modelBuilder.Entity<Models.Answers>().HasKey(c => c.AnswerID);
            modelBuilder.Entity<Models.Answers>().HasIndex(c => c.QuestionID);
            modelBuilder.Entity<Models.Answers>().HasIndex(c => c.QuestionChoiceID);            
            
            modelBuilder.Entity<Models.Answers>().HasIndex(p => p.SearchVector).ForNpgsqlHasMethod("GIN");
         
            modelBuilder.Entity<Models.Dict_Category>().HasKey(c => c.CategoryID);
            modelBuilder.Entity<Models.Dict_SubCategory>().HasKey(c => c.SubCategoryID);
            modelBuilder.Entity<Models.Dict_SubCategory>().HasIndex(c => c.CategoryID);
          



            modelBuilder.Entity<Models.LevelKnowledgeTable>().HasKey(c => c.LevelKnowledgeID);

            modelBuilder.Entity<Models.QuestionsView>().HasKey(c => c.QuestionID);
            modelBuilder.Entity<Models.Users>().HasKey(c => c.UserID);

            modelBuilder.Entity<Models.Quiz>().HasKey(c => c.QuizID);

            modelBuilder.Entity<Models.Quiz>().HasIndex(c => c.UserID);

            modelBuilder.Entity<Models.Quiz>().HasIndex(c => c.QuizModeID);

            modelBuilder.Entity<Models.Dict_QuizMode>().HasKey(c => c.QuizModeID);

            modelBuilder.Entity<Models.Dict_ResultsRange>().HasKey(c => c.ResultsRangeID);            

            modelBuilder.Entity<Models.QuizQuestionsDetail>().HasKey(c => c.QuizQuestionDetailID);

            modelBuilder.Entity<Models.QuestionsBlocking>().HasKey(c => c.QuizQuestionDetailID);

            modelBuilder.Entity<Models.Dict_CategoryQuestionView>().HasKey(c => c.SubCategoryID);

            modelBuilder.Entity<Models.V_DashboardQuizTopResults>().HasKey(c => c.QuizID);

            modelBuilder.Entity<Models.V_DashboardQuizSummaryInfo>().HasKey(c => c.QuizID);

            modelBuilder.Entity<Models.V_DashboardQuizTopQuestions>().HasKey(c => c.QuestionID);

            modelBuilder.Entity<Models.V_DashboardQuizQuestions>().HasKey(c => c.QuestionID);

            modelBuilder.Entity<Models.V_DashboardQuizProcentResults>().HasKey(c => c.QuizID);

            modelBuilder.Entity<Models.V_DashboardQuizTopSubCategory>().HasKey(c => c.UserID);

            modelBuilder.Entity<Models.V_DashboardQuizActivites>().HasKey(c => c.UserID);

            modelBuilder.Entity<Models.V_DashboardUsersComments>().HasKey(c => c.QuizQuestionsDetailCommentsID);

            //def. relacji

            modelBuilder.Entity<Models.Questions>().HasMany(c => c.QuestionsImageRel).WithOne(c => c.Quest).HasForeignKey(k=>k.QuestionID);

            modelBuilder.Entity<Models.Questions>().HasMany(c => c.QuestionsChoiceRel).WithOne(c => c.Question).HasForeignKey(k => k.QuestionID);

            modelBuilder.Entity<Models.V_DashboardQuizQuestions>().HasMany(c => c.QuestionsImageRel1).WithOne(c => c.V_DashboardQuizQuestions).HasForeignKey(k=>k.QuestionID);

            modelBuilder.Entity<Models.V_DashboardQuizQuestions>().HasMany(c => c.QuestionsChooseRel).WithOne(c => c.V_DashboardQuizQuestions).HasForeignKey(k => k.QuestionID);


            //tablica QuizQuestionsDetailComments 30.11.2019
            modelBuilder.Entity<Models.TTickets>().HasKey(c => c.QuizQuestionsDetailCommentsID);
           // modelBuilder.Entity<Models.TTickets>().HasMany(c=>c.Dict_CommentsTypeRel).WithOne(c=>c.)
         
        }

    }

    public class DbBiolContext : AppDBContext
    {
        public DbBiolContext(DbContextOptions<DbBiolContext> options) : base(options)
        {
        }
    }

    public class DbHistContext : AppDBContext
    {
        public DbHistContext(DbContextOptions<DbHistContext> options) : base(options)
        {
        }
    }



}
