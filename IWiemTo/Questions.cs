﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IWiemTo.Data;
using Microsoft.EntityFrameworkCore;
using IWiemTo.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace IWiemTo
{
    public class Questions: IQuestions, IDisposable
    {
        private bool _disposed = false;
        public Questions()
        {

        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {

                }
            }
            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        public List<int> BlockingQuestion(AppDBContext _appDbContext)
        {
            IQueryable<Models.QuestionsBlocking> questionsBlocking = null;
            List<int> blockQuestList = new List<int>();
            questionsBlocking = _appDbContext.QuestionsBlocking.AsNoTracking().AsQueryable().OrderBy(n => n.QuestionID);

            foreach (var item in questionsBlocking)
            {
                blockQuestList.Add(item.QuestionID);
            }
            return blockQuestList;
        }

        public void Load_ChooseQuestionModeImagesFromDB(Models.Quiz quizData, AppDBContext _appDbContext)
        {
            // jezeli wybrano wszystkie typy pytań to uzupełnij ID -ki

            if (quizData.QuestionsModeID == null)
            {
                var values1 = new int[5];
                for (int k = 0; k < 5; k++)
                {
                    values1[k] = k + 1;
                }
                quizData.QuestionsModeID = values1;
            }

            if (quizData.QuestionsModeID[0] == 0)
            {
                var values1 = new int[5];
                for (int k = 0; k < 5; k++)
                {
                    values1[k] = k + 1;
                }
                quizData.QuestionsModeID = values1;
            }

            List<Models.ModelQuestionsModeData> lst = new List<IWiemTo.Models.ModelQuestionsModeData>();

            var questionMode = _appDbContext.Dict_QuestionsType.Where(k => quizData.QuestionsModeID.Contains(k.TypeID)).ToList();
            foreach (var item in questionMode)
            {
                if (!string.IsNullOrEmpty(item.TypeNameImage))
                    lst.Add(new Models.ModelQuestionsModeData { Data1 = item.TypeNameImage, Data2 = item.TypeName });
                else
                    lst.Add(new Models.ModelQuestionsModeData { Data1 = "", Data2 = item.TypeName });

            }

            quizData.QuestionsModeData = lst;
        }


        public void Load_ChooseQuizModeImagesFromDB(Models.Quiz quizData, AppDBContext _appDbContext)
        {
            var defaultImage = "";
            List<string> lst = new List<string>();
            if ((quizData.QuizModeID == 0))
            {
                lst.Add(defaultImage);
            }
            else
            {
                var quizMode = _appDbContext.Dict_QuizMode.Where(k => k.QuizModeID == quizData.QuizModeID).ToList();
                foreach (var item in quizMode)
                {
                    if (!string.IsNullOrEmpty(item.QuizModeImage))

                        lst.Add(item.QuizModeImage);
                    else
                        lst.Add(defaultImage);
                }

            }
            quizData.QuizModeImage = lst;
        }

        public void Load_ChooseCategoriesImagesFromDB(Models.Quiz quizData, AppDBContext _appDbContext)
        {

            List<Models.ModelSubCategoryData> lst = new List<Models.ModelSubCategoryData>();
            if ((quizData.SubCategoryID == null))
            {
                lst.Add(new Models.ModelSubCategoryData { Data1 = "book.png", Data2 = "wszystkie kategorie" });
            }
            else
            {
                int countChoose = quizData.SubCategoryID.Length;
                var countAll = _appDbContext.Dict_SubCategory.Count();
                if (countAll == countChoose)
                {
                    lst.Add(new Models.ModelSubCategoryData { Data1 = "book.png", Data2 = "wszystkie kategorie" });
                }
                else
                {
                    var subCategories = _appDbContext.Dict_SubCategory.Where(k => quizData.SubCategoryID.Contains(k.SubCategoryID)).ToList();
                    foreach (var item in subCategories)
                    {
                        if (!string.IsNullOrEmpty(item.SubCategoryImage))

                            lst.Add(new Models.ModelSubCategoryData { Data1 = item.SubCategoryImage, Data2 = item.SubCategoryName });
                        else
                            lst.Add(new Models.ModelSubCategoryData { Data1 = "", Data2 = item.SubCategoryName });
                    }
                }
            }
            quizData.SubCategoryData = lst;
        }

        public IQueryable<V_DashboardQuizSummaryInfo> GetLastThreeQuizes(Models.Quiz quizData, AppDBContext _appDbContect)
        {
            IQueryable<V_DashboardQuizSummaryInfo> tb = _appDbContect.V_DashboardQuizSummaryInfo.Where(k => k.UserID == quizData.UserID).Take(3).AsQueryable();
            return tb;
        }


        public IEnumerable<QuestionsGeneralInfo> GetQuestionsGeneralInfo( List<AppDBContext> _appDbContext)
        {
            List<QuestionsGeneralInfo> myList = new List<QuestionsGeneralInfo>();
            QuestionsGeneralInfo info;
            foreach (var item in _appDbContext)
            {
                info = new QuestionsGeneralInfo()
                {
                    AllCount = (int)item.Questions.Count(),
                    ExtendedCount = (int)item.Questions.Where(k => k.QuestionExtended == 1).Count(),
                    BasicCount = (int)item.Questions.Where(k => k.QuestionExtended == 0).Count(),
                    UpdateDate = (DateTime)item.Questions.Max(k => k.QuestionCreated)
                };
                myList.Add(info);
            }
            IEnumerable<QuestionsGeneralInfo> lista = myList as IEnumerable<QuestionsGeneralInfo>;

            return lista;
        }

        //  pobranie ze słownika rodzajów komentarzy
       public IEnumerable<SelectListItem> Load_DictCommentsType(AppDBContext _appDbContext)
        {
           List<SelectListItem> commentsType = _appDbContext.Dict_CommentsType.AsNoTracking().OrderBy(n => n.CommentsType).Select
          (
            n => new SelectListItem { Value = n.CommentsTypeID.ToString(), Text = n.CommentsType, Selected = n.CommentsTypeID == 1 ? true : false }
          ).ToList();
            return commentsType;
        }

    }
}

