﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IWiemTo.Models;
using IWiemTo.Data;

namespace IWiemTo
{
    public interface IDashboard: IDisposable
    {      

        int GetCountAllQuizByUser(AppDBContext _appDbContext, int? userID);

        int GetCountAllPositiveQuizByUser(AppDBContext _appDbContext, int? userID);

        int GetCountAllNegativeQuizByUser(AppDBContext _appDbContext, int? userID);

        IEnumerable<V_DashboardQuizTopResults>   LoadQuizTop5Results(AppDBContext _appDbContext, int? userID) ;

        IEnumerable<V_DashboardQuizTopQuestions> LoadQuizTop5Questions(AppDBContext _appDbContext, int? userID);

        IEnumerable<V_DashboardQuizTopSubCategory> LoadQuizTop5SubCategory(AppDBContext _appDbContext, int? userID);

        IEnumerable<V_DashboardQuizProcentResults> LoadQuizProcentResultsByUser(AppDBContext _appDbContext, int? userID, int dateRange, DateTime dataFrom, DateTime dataTo);

        IEnumerable<Dict_ResultsRange> LoadDict_ResultsRange(AppDBContext _appDbContext, int? userID);

        IEnumerable<V_DashboardQuizActivites> LoadQuizActivites(AppDBContext _appDbContext, int? userID);

        List<ProcentResultsRange> GetTableProcentResults(AppDBContext _appDbContext, int? userID, int dateRange, DateTime dataFrom, DateTime dataTo);

        V_DashboardQuizQuestions ShowQuestion(int questionID, AppDBContext _appDbContext, int? userID);

        IQueryable<V_DashboardQuizQuestions> GetAllQuestionsByQuizID(AppDBContext _appDbContext,int quizID);


        /// <summary>
        /// 
        /// widok zwraca wszystkie komentarze dodane przez użytkowników
        /// dodane: 21.11.2019
        /// definicja metody w klasie Dashboard.cs
        /// </summary>
        /// <param name="_appDbContect"></param>
        /// <returns></returns>
        IQueryable<V_DashboardUsersComments> GetAllUsersComments(AppDBContext _appDbContext, int userID);


    }
}
