﻿using IWiemTo.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IWiemTo
{
    public class Tickets: ITickets , IDisposable
    {
        private bool disposed = false;
        private AppDBContext _appDBContext;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (_appDBContext != null)
                    {
                        _appDBContext.Dispose();
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IQueryable<Models.TTickets> GetAllUsersTickets(Data.AppDBContext _appDBContext)
        {
            return null;
        }
    }
}
