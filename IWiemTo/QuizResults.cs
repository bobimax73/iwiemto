﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IWiemTo.Models;
using IWiemTo.Data;

namespace IWiemTo
{
    public class QuizResults : IQuizResults, IDisposable
    {

        private bool disposed = false;
        private AppDBContext _appDBContext;
        public  QuizResult   _quizResult;

      
        public QuizResults (AppDBContext context )
        {
            _appDBContext = context;
            _quizResult   = new QuizResult();
        }

        public int  GetCountAllQuizByUser(int? userID )
        {
            if (userID != null)
            {
                int count = _appDBContext.Quiz.Where(c => c.UserID == userID && ( c.QuizStatus == 1)).Count();
                return count;
            }
            else
                return -1;
        }

        public int GetCountPositiveQuizByUser( int? userID)
        {
            if (userID != null)
            {
                int count = _appDBContext.Quiz.Where(c => c.UserID == userID && (c.QuizResult == 1 && c.QuizStatus == 1)).Count();
                return count;
            }
            else
                return -1;
        }


        public void QuizResultLoadData(int? userID)
        {
            if (userID != null)
            {
                _quizResult.QuizCount = _appDBContext.Quiz.Where(c => c.UserID == userID).Count();
               // _quizResult.QuizPositiveCount = _appDBContext.Quiz.Where(c => c.UserID == userID && (c.QuizResult == 1 && c.QuizStatus == 1)).Count();
               // _quizResult.QuizNegativeCount = _appDBContext.Quiz.Where(c => c.UserID == userID && (c.QuizResult == 0 && c.QuizStatus == 1)).Count();
            }            
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _appDBContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
