﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc;
using IWiemTo.Data;

namespace IWiemTo
{
    public interface IQuestionsEditor :IDisposable
    {
        // utworz liste rodzajów tresci pytania
        IEnumerable<SelectListItem> LoadTypeOfQuestionContent(AppDBContext _appDbContext);

        // pobranie ze słownika listy kategorii
        IEnumerable<SelectListItem> GetCategories(AppDBContext _appDbContext);

        //pobranie ze słownika listy podkategorii
        IEnumerable<SelectListItem> GetSubCategories(AppDBContext _appDbContext);

        //pobranie ze słownika listy podkategorii ograniczonyej do wybranej kategorii
        IEnumerable<SelectListItem> GetSubCategoriesByCategoryID(AppDBContext _appDbContext, int? categoryID);

        IEnumerable<SelectListItem> GetLevelKnowledge(AppDBContext _appDbContext);

        

    }
}
