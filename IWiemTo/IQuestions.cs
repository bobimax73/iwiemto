﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IWiemTo.Data;
using IWiemTo.Models;
using Microsoft.AspNetCore.Mvc.Rendering;


namespace IWiemTo
{
    public interface IQuestions : IDisposable
    {
        /// <summary>
        /// Lista pytań zablkowanych - niedostępnych w puli pytań
        /// </summary>
        /// <returns></returns>
        List<int> BlockingQuestion( AppDBContext _appDbContext );

        /// <summary>
        /// Pobranie listy obrazków wybranego rodzaju pytań
        /// Jezeli nie została wybrana żadna opcja  lub zostały wybrane wszytskie rodzaje pytań - do listy wstawiany jest domylsny obrazek
        /// </summary>
        /// <param name="quizData"></param>
        /// <returns></returns>
        /// 
        void Load_ChooseQuestionModeImagesFromDB(Models.Quiz quizData, AppDBContext _appDbContext);


        /// <summary>
        /// Pobranie listy obrazków  dla trybu quizu
        /// Jezeli nie została wybrany zaden tryb (sytuacja dziwna) - do listy wstawiany jest domylsny obrazek - pusty
        /// </summary>
        /// <param name="quizData"></param>
        /// <returns></returns>
        void Load_ChooseQuizModeImagesFromDB(Models.Quiz quizData, AppDBContext _appDbContext);

        /// <summary>
        /// Pobranie listy obrazków wybranych podkategorii
        /// Jezeli nie została wybrana żadna podkategoria lub zostały wybrane wszytskie podkategorie - do listy wstawiany jest domylsny obrazek
        /// </summary>
        /// <param name="quizData"></param>
        /// <returns></returns>
        void Load_ChooseCategoriesImagesFromDB(Models.Quiz quizData, AppDBContext _appDbContext);

        /// <summary>
        /// Pobranie danych o trzech ostatnich Quizach
        /// </summary>
        IQueryable<V_DashboardQuizSummaryInfo> GetLastThreeQuizes(Models.Quiz quizData, AppDBContext _appDBContext);


        IEnumerable<QuestionsGeneralInfo> GetQuestionsGeneralInfo( List<AppDBContext> _appDbContext);

        //  pobranie ze słownika rodzajów komentarzy
        IEnumerable<SelectListItem> Load_DictCommentsType(AppDBContext _appDbContext);

    }
}
