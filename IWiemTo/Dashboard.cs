﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IWiemTo.Models;
using IWiemTo.Data;

namespace IWiemTo
{
    public class Dashboard : IDashboard, IDisposable
    {
        private bool disposed = false;
        public int ?   _userID;
        private int _countUserQuiz;
        private int _countPositiveUserQuiz;
        private int _countNegativeUserQuiz;
        private AppDBContext _appDBContext;

        // liczba wykonanych quizow
        public int CountUserQuiz
        {
            get
            {
                return _countUserQuiz;
            }
            set
            {
                _countUserQuiz = GetCountAllQuizByUser(_appDBContext,_userID);
            }
        }

        public int CountPositiveUserQuiz
        {
            get { return _countPositiveUserQuiz; }
            set { _countPositiveUserQuiz = GetCountAllPositiveQuizByUser(_appDBContext, _userID); }
        }


        public int CountNegativeUserQuiz
        {
            get { return _countNegativeUserQuiz; }
            set { _countNegativeUserQuiz = GetCountAllNegativeQuizByUser(_appDBContext,_userID); }
        }

        public Dashboard()
        {

        }

        public Dashboard(AppDBContext _appDbContext, int? userID)
        {
            _appDBContext=_appDbContext;
            _userID = userID;
        }


        /// <summary>
        /// metoda zwraca całkowita liczbe quizów wykonanych przez usera
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public int GetCountAllQuizByUser(AppDBContext _appDbContext, int? userID)
        {
            if (userID != null)
            {
                
                var count = _appDbContext.Quiz.Where(k => k.UserID == userID && k.QuizStatus == 1).Count();
                return count;
            }
            else
                return 0;         
        }

        /// <summary>
        /// metoda zwraca całkowita Pozytywna liczbe wykonanych quizów przez usera
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public int GetCountAllPositiveQuizByUser(AppDBContext _appDbContext, int? userID)
        {
            if (userID != null)
            {
                var count = _appDbContext.Quiz.Where(k => k.UserID == userID && k.QuizStatus == 1 && k.QuizResult == 1).Count();
                return count;
            }
            else
                return 0;
        }

        /// <summary>
        /// metoda zwraca całkowita Negatywna liczbe wykonanych quizów przez usera
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public int  GetCountAllNegativeQuizByUser(AppDBContext _appDbContext, int? userID)
        {
            if (userID != null)
            {
                var count = _appDbContext.Quiz.Where(k => k.UserID == userID && k.QuizStatus == 1 && k.QuizResult == 0).Count();
                return count;
            }
            else
                return 0;
        }


        public IEnumerable<V_DashboardQuizTopResults> LoadQuizTop5Results(AppDBContext _appDbContext, int? userID)
        {
            IEnumerable<V_DashboardQuizTopResults> quizTop5Results = null;
            quizTop5Results = _appDbContext.V_DashboardQuizTopResults.Where(k => k.UserID == userID && k.Procent >=60).Select(c =>
                new V_DashboardQuizTopResults
                {
                    QuizID = c.QuizID,
                    QuizCreateDate = c.QuizCreateDate,
                    QuizName = c.QuizName,
                    QuizAllScore = c.QuizAllScore,
                    QuizModeName = c.QuizModeName,
                    QuizQuestionCount = c.QuizQuestionCount,
                    Procent = c.Procent,
                    UserScore              = c.UserScore,
                    UserID                 = c.UserID,
                    QuestionsCountPositive = c.QuestionsCountPositive,
                    QuestionsCountNegative = c.QuestionsCountNegative,
                    QuizDescribe           = c.QuizDescribe
                }
                ).Take(5).AsQueryable();
          
            return quizTop5Results;
        }


        public IEnumerable<V_DashboardQuizTopQuestions> LoadQuizTop5Questions(AppDBContext _appDbContext, int? userID)
        {
            IEnumerable<V_DashboardQuizTopQuestions> quizTop5Questions = null;
            quizTop5Questions = _appDbContext.V_DashboardQuizTopQuestions.Where(k => k.UserID == userID).Select(c =>
                 new V_DashboardQuizTopQuestions
                 {
                     QuestionID     = c.QuestionID,
                     QuestionText   = c.QuestionText,
                     QuestionCount  = c.QuestionCount,
                     UserID         = c.UserID,
                     TypeName       = c.TypeName
                 }
                ).Take(5).AsQueryable();
            return quizTop5Questions;
        }


        public IEnumerable<V_DashboardQuizTopSubCategory> LoadQuizTop5SubCategory(AppDBContext _appDbContext, int? userID)
        {
            IEnumerable<V_DashboardQuizTopSubCategory> quizTop5SubCategory = null;
            quizTop5SubCategory = _appDbContext.V_DashboardQuizTopSubCategory.Where(k => k.UserID == userID).Select(c =>
                 new V_DashboardQuizTopSubCategory
                 {
                     UserID          = c.UserID,
                     SubCategoryName = c.SubCategoryName,
                     QuestionCount   = c.QuestionCount,
                     SubCategoryImage= c.SubCategoryImage
                 }
                ).OrderBy(c=>c.QuestionCount).Take(5).AsQueryable();
            return quizTop5SubCategory;
        }


        public IEnumerable<V_DashboardQuizActivites> LoadQuizActivites(AppDBContext _appDbContext, int? userID)
        {
            IEnumerable<V_DashboardQuizActivites> quizActivites = null;
            quizActivites = _appDbContext.V_DashboardQuizActivites.Where(k => k.UserID == userID).Select(c =>
                 new V_DashboardQuizActivites
                 {
                     UserID = c.UserID,
                     QuizCreateDate = c.QuizCreateDate,
                     QuizCount = c.QuizCount
                 }
                ).OrderBy(c => c.QuizCreateDate).AsQueryable();
            return quizActivites;

        }


        public IEnumerable<V_DashboardQuizProcentResults> LoadQuizProcentResultsByUser(AppDBContext _appDbContext, int? userID,  int dateRange, DateTime dataFrom, DateTime dataTo)
        {
            IEnumerable<V_DashboardQuizProcentResults> quizProcentResults = null;
           quizProcentResults = _appDbContext.V_DashboardQuizProcentResults.Where(k => k.UserID == userID).Select(c =>
                 new V_DashboardQuizProcentResults
                 {
                     QuizID  = c.QuizID,
                     UserID  = c.UserID,
                     Procent = c.Procent,
                     QuizData = c.QuizData
                 }
                ).AsQueryable();
            if ( dateRange == 3 )  quizProcentResults = quizProcentResults.Where(z=>z.QuizData == DateTime.Today);
            if ( dateRange == 1 )  quizProcentResults = quizProcentResults.Where(z=>z.QuizData.Month == DateTime.Today.Month);
            if (dateRange == 2) quizProcentResults = quizProcentResults.Where(z => z.QuizData>= dataFrom.Date && z.QuizData<=dataTo.Date);

            return quizProcentResults;
        }


        public IEnumerable<Dict_ResultsRange> LoadDict_ResultsRange(AppDBContext _appDbContext, int? userID)
        {
            IEnumerable<Dict_ResultsRange> dict_ResultsRange = null;
            dict_ResultsRange = _appDbContext.Dict_ResultsRange.Select(c =>
                  new Dict_ResultsRange
                  {
                     ResultsRangeID  = c.ResultsRangeID,
                     RangeName       = c.RangeName,
                     ResultMinValue = c.ResultMinValue,
                     ResultMaxValue = c.ResultMaxValue
                  }
                 ).OrderBy(c=>c.ResultsRangeID).AsQueryable();
            return dict_ResultsRange;

        }


        public V_DashboardQuizQuestions  ShowQuestion(int questionID, AppDBContext _appDbContext, int? userID)
        {

            V_DashboardQuizQuestions question = null;
            question = _appDbContext.V_DashboardQuizQuestions.Where(k => k.QuestionID == questionID).Select(c =>
                 new V_DashboardQuizQuestions
                 {
                       QuestionID        = c.QuestionID,
                       QuizID            = c.QuizID,
                       SubCategoryID     = c.SubCategoryID,
                       QuestionPoints    = c.QuestionPoints,
                       QuestionText      = c.QuestionText,
                       CategoryName      = c.CategoryName,
                       SubCategoryName   = c.SubCategoryName,
                       QuestionSource    = c.QuestionSource,
                       SubQuestionCount  = c.SubQuestionCount,
                       QuestionExtended  = c.QuestionExtended,
                       TypeName          = c.TypeName,
                       TypeID            = c.TypeID,
                       QuestionsImageRel1 = c.QuestionsImageRel1.Select(k => new Models.QuestionsImage  { ImageID = k.ImageID, Image = k.Image }).ToList(),
                       QuestionsChooseRel = c.QuestionsChooseRel.Select(k => new Models.QuestionsChoice { QuestionID = k.QuestionID, IsCorect =k.IsCorect, ChoiceText=k.ChoiceText }).ToList()
                 }
                ).FirstOrDefault();
            return question;
        }

        public List<ProcentResultsRange> GetTableProcentResults(AppDBContext _appDbContext, int? userID, int dateRange, DateTime dataFrom, DateTime dataTo)
        {
            List<ProcentResultsRange> tab = new List<ProcentResultsRange>();

            var dict_range = LoadDict_ResultsRange(_appDbContext, userID);
            var data       = LoadQuizProcentResultsByUser( _appDbContext, userID, dateRange, dataFrom, dataTo);

            if ( (dict_range != null ) && (data != null))
            {
                foreach( var item in dict_range)
                {
                    int count = data.Where(c => (c.Procent >= item.ResultMinValue && c.Procent <= item.ResultMaxValue)).Count();
                    tab.Add(new ProcentResultsRange { RangeName = item.RangeName, ProcentResultCount = count });
                }
               
            }
        
            return tab;
        }

        //dodane 29.08.2019
        // pobiera wszystkie pytania dla id quizu

        public IQueryable<V_DashboardQuizQuestions> GetAllQuestionsByQuizID(AppDBContext _appDbContext, int quizID)
        {
            IQueryable<V_DashboardQuizQuestions> questsList = _appDbContext.V_DashboardQuizQuestions.Where(k => k.QuizID == quizID).Select(
                q=> new Models.V_DashboardQuizQuestions {
                    QuestionID         = q.QuestionID,
                    QuestionText       = q.QuestionText,
                    QuizID             = q.QuizID,
                    QuestionPoints     = q.QuestionPoints,
                    CategoryName       = q.CategoryName,
                    SubCategoryName    = q.SubCategoryName,
                    TypeName           = q.TypeName,
                    TypeID             = q.TypeID,
                    IsCorect           = q.IsCorect,
                    Score              = q.Score,
                    QuestionsImageRel1 = q.QuestionsImageRel1.Select(c => new Models.QuestionsImage { ImageID = c.ImageID, Image = c.Image }).ToList(),
                    QuestionsChooseRel = q.QuestionsChooseRel.Select(c => new Models.QuestionsChoice
                    {
                        QuestionChoiceID = c.QuestionChoiceID,
                        ChoiceText       = c.ChoiceText
                    }).OrderBy(c => c.QuestionChoiceID).ToList()
                }).AsQueryable();
            return questsList;
        }

        //dodane 21.11.2019
        //widok zwraca wszystkie komentarze dodane przez użytkowników

        public IQueryable<V_DashboardUsersComments> GetAllUsersComments(AppDBContext _appDbContext, int userID)
        {
            IQueryable<V_DashboardUsersComments> usersCommentsList = null;
            usersCommentsList = _appDbContext.V_DashboardUsersComments.Where(c => c.UserID == userID).OrderByDescending(c => c.DateCreate).AsQueryable();
            return usersCommentsList;
        }



        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (_appDBContext != null)
                    {
                        _appDBContext.Dispose();
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
