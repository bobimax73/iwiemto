﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IWiemTo.Data;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace IWiemTo
{
    public class QuizNew : IQuizNew, IDisposable
    {
        private bool _disposed = false;



        public QuizNew()
        {

        }



        public IEnumerable<SelectListItem> Load_DictQuizMode(AppDBContext _appDbContext)
        {
            List<SelectListItem> quizMode = _appDbContext.Dict_QuizMode.Where(n => n.QuizModeName.ToUpper() != "MATURA").
                AsNoTracking().OrderBy(n => n.QuizModeName).Select
            (
              n => new SelectListItem { Value = n.QuizModeID.ToString(), Text = n.QuizModeName, Selected = n.QuizModeID == 1 ? true : false }
            ).ToList();
            return quizMode;
        }

        public List<SelectListItem> Load_DictQuestionType(AppDBContext _appDbContext)
        {
            List<SelectListItem> dict_questionType = _appDbContext.Dict_QuestionsType.AsNoTracking().OrderBy(n => n.TypeID).Select
                 (
                     n => new SelectListItem { Value = n.TypeID.ToString(), Text = n.TypeName + "(" + _appDbContext.Questions.Where(c => c.QuestionTypeID == n.TypeID).Count() + ")" }
                 ).ToList();

            var addTypeNew = new SelectListItem() { Value = "0", Text = "wszystkie rodzaje pytań", Selected = true };
            dict_questionType.Insert(0, addTypeNew);
            return dict_questionType;

        }


        // pobranie słownika Poziomu wiedzy - szkoła podstawowa szkoła średnia
        public List<SelectListItem> Load_DictLavelKnowledge(AppDBContext _appDbContext)
        {
            List<SelectListItem> dict_levelknowledge = _appDbContext.LevelKnowledgeTable.AsNoTracking().OrderBy(n => n.LevelKnowledgeID).
                 Select(n => new SelectListItem { Value = n.LevelKnowledgeID.ToString(), Text = n.LevelKnowledgeName, Selected = n.LevelKnowledgeID == 2 ? true : false }).ToList();
            return dict_levelknowledge;
        }


    public List<SelectListItem> Load_DictCategoryView( AppDBContext _appDbContext )
        {

            List<SelectListItem> dict_CategoryView = new List<SelectListItem>();

            SelectListGroup category = new SelectListGroup();

            List<Models.Dict_CategoryQuestionView> db_dict_CategoryView = _appDbContext.Dict_CategoryQuestionView.AsNoTracking().ToList();
            var optionCategory = "";
            foreach (var item in db_dict_CategoryView)
            {
                var tmp = item.CategoryName;
                var subCategoryName = "";
                if (optionCategory != tmp)
                {
                    optionCategory = tmp;
                    category = new SelectListGroup() { Name = item.CategoryName };

                }
                var countQuestsSubCategory = _appDbContext.QuestionCategory.Where(c => c.CategorySubID == item.SubCategoryID).Count();
                subCategoryName = item.SubCategoryName + "(" + countQuestsSubCategory.ToString() + ")";
                dict_CategoryView.Add(new SelectListItem { Value = item.SubCategoryID.ToString(), Text = subCategoryName, Group = category });
            }


            return dict_CategoryView;
        }



        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {

                }
            }
            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
