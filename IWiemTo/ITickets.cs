﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace IWiemTo
{
    public interface ITickets : IDisposable
    {
         // funkcja pobiera wszytskie tickety uzytkowników
         // dodana 01.12.2019
         IQueryable<Models.TTickets> GetAllUsersTickets(Data.AppDBContext _appDBContext);
    }
}
