﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using IWiemTo.Models;
using IWiemTo.Data;

namespace IWiemTo.Controllers
{
    public class HomeController : Controller
    {
        private DbBiolContext _appDBContext;
        private DbHistContext _appDBContext2;
        private IQuestions _questions;
        private List<AppDBContext> listAppDBContext = new List<AppDBContext>();
        public HomeController(DbBiolContext context, DbHistContext contextHis,  IQuestions questions)
        {
            _appDBContext  = context;
            _appDBContext2 = contextHis;
            _questions    = questions;
            listAppDBContext.Add(_appDBContext);
            listAppDBContext.Add(_appDBContext2);
        }

        public IActionResult Index()
        {

            var z = _questions.GetQuestionsGeneralInfo(listAppDBContext);
            return View(z);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
