﻿
$(document).ready(function ()
{
    $('#').DataTable(
        {
            'oLanguage' : { sProcessing: "<img id='sProcessing' src='/images/ajax-loader.gif'>", sUrl: "/lib/DataTables/Polish.json" },
            'processing': true, 
            'serverSide': true,
            'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, "All"]],
            'ajax':
            {
                'url' : '/Tickets/showAllUserComments', // wywołaj metode showAllUserComments z kontrolera TicketsController
                'type': 'POST',
                'datatype': 'JSON'                
            }
        }
    );


});
