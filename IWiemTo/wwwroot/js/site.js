﻿// Write your JavaScript code.
$(document).ready(function () {
    var areaName = $('#Area').text();
    $('#SubmitQuiz').click(function () {


        var questionCount = $(this).data().questionscount;    // liczba pytań Qizu 
        var question = {};                                    // obiekt reprezentujacy question wszystkie dane 
        var resultQuiz = [];                                  // tablica dla obiektów question  
        var j = 1;
        var questionchoiceID = 0;
        for (var k = 1; k < questionCount; k++) {
            var useranswer = [];                              // tablica odpowiedzi udzielonych przez użytkownika
            var questionChoiceId = [];                        // tablica identyfikatorów podzapytań 

            $(':input').each(function () {

                var $this = $(this);
                if ($this.is("input")) {

                    if ($this.attr('name') == 'Quest_' + k) {

                        $.each($('input[name=Quest_' + k + ']:checked'), function () {
                            useranswer.push($(this).val());

                        });
                        return false;
                    }
                }



                if ($this.is("textarea")) {

                    if ($this.attr('name') == 'Quest_' + k) {
                        $('textarea[name=Quest_' + k + ']').each(function () {
                            var idTextarea = $(this).attr('id');
                            var choiceId = $('#Span_' + idTextarea).text();
                            if ($(this).val().length !== 0) {
                                useranswer.push($(this).val());
                            }
                            else
                                useranswer.push(' ');
                            questionChoiceId.push(choiceId);
                        });

                        return false;
                    }
                }


                if ($this.is("select")) {

                    if ($this.attr('name') == 'Quest_' + k) {
                        $('select[name=Quest_' + k + ']').each(function () {
                            var idSelect = $(this).attr('id');
                            var choiceId = $('#Span_' + idSelect).text();
                            useranswer.push($(this).val());
                            questionChoiceId.push(choiceId);
                        });

                        return false;
                    }
                }

            });

            question = {
                'QuestionID': $('#Q_ID' + k).text(),
                'QuestionPoints': $('#Q_ID' + k).data().questionpoints,
                'QuestionScore': 0,
                'QuestionText': $('#Pytanie' + k).text(),
                'QuestionTypeID': $('#Q_Type_ID' + k).text(),
                'QuizID': $('#Q_ID' + k).data().quizid,
                'UserID': $('#Q_ID' + k).data().userid,
                'QuizDate': $('#Q_ID' + k).data().quizdate,
                'Answer': useranswer,
                'QuestionChoiceID': questionChoiceId,
                'ElementID': k
            };
            resultQuiz.push(question);
        }
        $('#SubmitQuiz').prop('disabled', true);
        showProgress();
        $.ajax({
            url: "/" + areaName + "/Questions/GetAnswers",
            type: "POST",
            dataType: "json",
            data: { resultQuiz },
            success: function (response) {

                hideProgress();

                var scoreUser = [];
                var scoreQuiz = [];
                var countCorectAnswer = 0;
                var countNotCorectAnswer = 0;

                for (var z = 0; z < response.length; z++) {
                    scoreUser.push(response[z].questionScore);
                    scoreQuiz.push(response[z].questionPoints);
                    if (response[z].isCorect == true) {
                        countCorectAnswer = countCorectAnswer + 1;
                        var punktacja = 'pkt. ' + response[z].questionScore + '|' + response[z].questionPoints;
                        var AnsQtext = `<div class="alert alert-success" role="alert">
                                        <span class="glyphicon glyphicon-thumbs-up" aria - hidden="true" ></span > 
                                        <span style = "float:right;">`+ punktacja + `</span> 
                                        Poprawna odpowiedź
                                      </div>`
                        $('#AnsQ' + response[z].elementID).html(AnsQtext);
                        //$('#AnsQ' + response[z].elementID).html('<div class="alert alert-success" role="alert"><span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span>' + '<span style="float:right;">' + punktacja + '</span>' + 'Poprawna odpowiedź'+'</div>');

                    }
                    else {
                        countNotCorectAnswer = countNotCorectAnswer + 1;
                        var punktacja = 'pkt. ' + response[z].questionScore + '|' + response[z].questionPoints;
                        var AnsQtext = '<div class="alert alert-danger" role="alert">' +
                            '<span class="glyphicon glyphicon-thumbs-down" aria-hidden="true"></span>Błędna odpowiedź </span>' + '<span style = "float:right;margin-left:5px">' + punktacja + '</span>' +
                            '</div>' +
                            '<div id=DivOptionButtons' + response[z].questionID + '>' +
                            '<a  href="#"  class="btn btn-primary" id="ShowCorectAnswer" ' + ' data-questionid=' + response[z].questionID + ' data-elementid=' + response[z].elementID + ' data-iscorectquest=' + response[z].isCorectJson + '> Pokaż poprawne odpowiedzi</a>' +
                            '<a  href="#"  style="margin-left:5px" class="btn btn-primary" id="SendComments" ' + ' data-questionid=' + response[z].questionID + ' data-quizid=' + response[z].quizID + ' data-elementid=' + response[z].elementID + ' data-iscorectquest=' + response[z].isCorectJson + '> Zgłoś uwagi</a>' +
                            '</div> ';
                        $('#AnsQ' + response[z].elementID).html(AnsQtext);





                        //$('#AnsQ' + response[z].elementID).html('<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-thumbs-down" aria-hidden="true"></span> Błędna odpowiedź </span > <a href="#" class="ShowCorectAnswer" ' + 'id = ' + 'ShowCorectAnswer_' + response[z].elementID + ' data-questionid=' + response[z].questionID + ' data-elementid=' + response[z].elementID + ' data-iscorectquest=' + response[z].isCorectJson +' > pokaż poprawne odpowiedzi</a > </div > ');  
                    }
                    j++;
                }
                var scoreUserSum = 0;
                var scoreQuizSum = 0;
                var scoreProcent = 0;

                for (var i = 0; i < scoreUser.length; i++) {
                    scoreUserSum = scoreUserSum + scoreUser[i];

                }
                for (var i = 0; i < scoreQuiz.length; i++) {
                    scoreQuizSum = scoreQuizSum + scoreQuiz[i];
                }
                scoreProcent = Math.floor((scoreUserSum / scoreQuizSum) * 100);
                if (scoreProcent >= 60) {

                    $('#Wynik').html(quizResultMessagePositive(scoreUserSum, scoreQuizSum, scoreProcent, countCorectAnswer, countNotCorectAnswer));
                    var current = parseInt($('#QuizPositivCount').text());
                    $('#QuizPositivCount').text(current + 1);

                }
                else {

                    $('#Wynik').html(quizResultMessageNegative(scoreUserSum, scoreQuizSum, scoreProcent, countCorectAnswer, countNotCorectAnswer));
                }

                $('html, body').animate({
                    scrollTop: $("#Area").offset().top
                }, 500);

                var pozycja = $('#SubmitQuiz').position();
               
               
                $('#btnSaveQuiz').toggle(true);
            }
        });
    });

    $('#btnSaveQuiz').click(function () {
        var url = window.location.href;          
        var quizname = $('#divQuizName').text();            
        quiz = { 'url': url, 'name': quizname};        
        $.ajax(
            {
                url: '/' + areaName + '/Questions/SaveQuizToPDF',
                type: 'POST', 
                data: { 'url' : url, 'quizname' : quizname},
                success: function (response) {
                    bootbox.alert('Quiz został zapisany!'); 
                    var blob = new Blob([response]);
                 
                  //  link.href = window.URL.createObjectURL(blob);

                    const data = window.URL.createObjectURL(blob);
                    var link = document.createElement('a');
                    link.href = data;
                    link.download = "dupa.pdf";
                    link.click();
                    setTimeout(function () {
                        // For Firefox it is necessary to delay revoking the ObjectURL
                        window.URL.revokeObjectURL(data);
                    }, 100);
                    window.location.href = window.URL.createObjectURL(blob);
                },
                error: function (response) { alert("Bład w trakcie wykonywania requestu!"); } 
            });       
    });

    function quizResultMessageNegative(scoreUserSum, scoreQuizSum, scoreProcent, countCorectAnswer, countNotCorectAnswer) {

        var messageNoPositive =
            `<div class="alert alert-danger" role="alert">
                <span><i  style='margin-top:1%'class="far fa-frown fa-3x"></i></span>               
                <h2 class="quiz_title" style='display:inline; font-weight: bold; margin-left:3%'> Twój wynik to <span>` + scoreUserSum + `/` + scoreQuizSum + ` pkt.</span></h2>` +
            `<div class="quiz_cl">
                   <div class="score_interpretation">
                    <p style='margin-left:10%; margin-top:1%' class="interpretation_text">
                        To niestety nie jest dobry wynik. Może spróbujesz ponownie w nowym quzie?</a>
                        <br>Tutaj znajdziesz <a href="#" class="ShowQuizDetail" style="font-weight:bold" data-scoreUserSum=`+ scoreUserSum + ` data-scoreQuizSum=` + scoreQuizSum + ` data-scoreProcent=` + scoreProcent + ` data-countCorectAnswer=` + countCorectAnswer + ` data-countNotCorectAnswer=` + countNotCorectAnswer +
            `>szczegóły</a> Twojego wyniku.
                    </p>
                    </div>
                </div>
            </div>
            `;
        return messageNoPositive;
    }


    function quizResultMessagePositive(scoreUserSum, scoreQuizSum, scoreProcent, countCorectAnswer, countNotCorectAnswer) {

        var messageNoPositive =
            `<div class="alert alert-success" role="alert">
                 <span><i  style='margin-top:1%'class="far fa-smile fa-3x"></i></span>
                <h2 class="quiz_title" style='display:inline; font-weight: bold; margin-left:3%'> Twój wynik to <span>` + scoreUserSum + `/` + scoreQuizSum + ` pkt.</span></h2>` +
            `<div class="quiz_cl">
                   <div class="score_interpretation">
                    <p style='margin-left:10%; margin-top:1%' class="interpretation_text">
                        Doskonale! Quiz zaliczony. 
                        </br>
                        Niewiele osób osiągnęło tak dobry wynik jak Ty. Jestem pod wrażeniem - masz dużą wiedzę. Spróbuj rozwiązać kolejny quiz.
                        <br>Tutaj znajdziesz <a href="#" class="ShowQuizDetail" style="font-weight:bold" data-scoreUserSum=`+ scoreUserSum + ` data-scoreQuizSum=` + scoreQuizSum + ` data-scoreProcent=` + scoreProcent + ` data-countCorectAnswer=` + countCorectAnswer + ` data-countNotCorectAnswer=` + countNotCorectAnswer +
            `>szczegóły</a>,Twojego wyniku.
                    </p>
                    </div>
                </div>
            </div>
            `;
        return messageNoPositive;
    }

    /* 
        Okno modalne ze szczegółami wyniku Quizu 
             
     */

    $('body').on('click', 'a.ShowQuizDetail', function () {
        var x = $(this);
        var result = {
            'scoreUserSum': $(this).data().scoreusersum,             // wynik - liczba zdobytych punktów przez usera
            'scoreQuizSum': $(this).data().scorequizsum,             // wynik - liczba wszytskich punktów, które można dostac w quiz
            'scoreProcent': $(this).data().scoreprocent,            //  wynik - wyliczony procentowy
            'countCorectAns': $(this).data().countcorectanswer,       //  wynik - ilosc poprawnych udzielonych poprawnych odpowiedzi
            'countNotCorectAns': $(this).data().countnotcorectanswer    //   wynik - ilosc niepoprawnych udzielonych odpowiedzi 
        };
        $.ajax({
            url: "/" + areaName + "/Questions/ShowDetailResult",
            type: "GET",
            data: result,
            dataType: "Html",
            success: function (response) {

                $('.modal-content').html(response);
                $('#myModalQuizResultDetail').modal('show');
            }
        });

    });

    $('body').on('click', 'a#ShowCorectAnswer.btn.btn-primary', function () {

        var question = {}, listOfQuestion = [], countOfElementRadioButton = 0;
        var dupa = $(this);
        var questionID = $(this).data().questionid;
        var elementID = $(this).data().elementid;
        var isCorectQuest = $(this).data().iscorectquest;

        question = {
            QuestionID: questionID,
            QuestionText: $('#Pytanie' + elementID).text(),
            AnswerText: '',
            ElementID: elementID,
            IsCorectSubQuest: isCorectQuest
        }

        $.ajax({
            url: "/" + areaName + "/Questions/GetAllAnswersNew",
            type: "POST",
            dataType: "json",
            data: question,
            success: function (response) {
                console.log(response);

                if (response !== null) {

                    ShowAllCorrectAnswer(response);
                }
            }
        });

    });

    $('body').on('click', 'a#SendComments.btn.btn-primary', function () {
        var questionID = $(this).data().questionid;
        var elementID = $(this).data().elementid;
        var quizID = $(this).data().quizid;
        var nameDiv = '#DivOptionButtons' + questionID;
        var isCorectQuest = $(this).data().iscorectquest;

        $.ajax({
            url: "/" + areaName + "/Questions/ShowCommentsForm",
            typ: "GET",
            dataType: "HTML",
            data: { QuestionID: questionID, QuizID: quizID },
            success: function (response) {
                //alert('ok');
                $(nameDiv).append(response);
            },
            error: function () {
                alert("Bład w trakcie ładowania danych!");
            }

        });
        //  alert('Ta funkcja jest w trakcie tworzenia....v');

    });

    // 20.09.2019
    //dodanie komentarza do pytania
    $('body').on('click', 'button#btnSendComments.btn.btn-info.btn-block', function () {

        var id = $('#QuizQuestionDetailID').val();
        var comment = $('#Comments').val();
        var commentid = $('#CommentsTypeID').val();
        $.ajax({
            url: "/" + areaName + "/Questions/AddCommentsToQuestion",
            type: "POST",
            data: { 'id': id, 'commentid': commentid, 'comment': comment },
            success: function (response) {
                bootbox.alert('Twój komentarz został dodany! Wkrótce otrzymasz odpowiedź.');
                $('#divComments').remove();
            },
            error: function () {
                alert("Bład w trakcie ładowania danych!");
            }

        });
    });


    $('body').on('click', 'button#btnCancelComments.btn.btn-warning.btn-block' , function () {

        $('#divComments').remove();
    });


    function ShowAllCorrectAnswer(response) {

      //// petla przez elementy Html
        
        $(':input').each(function () {
            var z = 0;
            var $elemHtml = $(this);
            var $answers = response;
            if ($elemHtml.is("textarea") || $elemHtml.is("select") )
                {
                   
                    for (var k = 0; k < $answers.length; k++) {
                        if (($elemHtml.attr('id') == 'Quest_choice' + $answers[k].elementID+''+z)) {
                            if ($answers[k].isCorect == false) {
                                if ($('div[name=Key_' + $answers[k].elementID + '' + z + ']').length) {
                                    $('div[name=Key_' + $answers[k].elementID + '' + z + ']').remove();
                                }

                                var klucz = '<p>' + 'Odpowiedź powinna zawierać  ' + '<strong class="text-success">' + $answers[z].answerText + '</strong>' + '</p>';
                                var el = $('<div class="alert alert-info" role="alert"><span class="glyphicon glyphicon-education" aria-hidden="true"> Klucz odpowiedzi </span>' + klucz + '</div>');
                                el.attr('name', 'Key_' + $answers[k].elementID + '' + z);

                                //// var div = $('div[id=AnsQ' + $answers[k].elementID + ']');
                                var div = $('div[id=Div_textarea' + $answers[k].elementID + '' + z + ']');
                                div.append(el);
                            }
                            else
                            {
                                var name = 'Info_' + $answers[k].elementID + '' + z;
                                if ($('div[name=' + name + ']').length) $('div[name=' + name + ']').remove();
                                    
                                var id = 'Div_textarea' + $answers[k].elementID + '' + z;
                                var htmlDIVTextarea = $('div[id=' + id + ']'); 
                                var htmlDIVOk = $('<span class="glyphicon glyphicon-thumbs-up" aria-hidden="true">       </span>');
                              
                                htmlDIVOk.attr('name', name);
                                htmlDIVTextarea.append(htmlDIVOk);
                            }
                        }
                         z++;
                    }
                    
                   
                }
                if ($elemHtml.is("input")) {

                    for (k = 0; k < $answers.length; k++) {
                        countOfElementRadioButton = $('input[name=Quest_' + $answers[k].elementID + ']').length;
                        for (var item = 0; item < countOfElementRadioButton; item++) {
                            var valElementRadioButton = $('#Quest_choice' + $answers[k].elementID + item).val().toLowerCase();
                            var result = valElementRadioButton.localeCompare($answers[k].answerText.toLowerCase());
                            if (result == 0) {
                                $('#Quest_choice' + $answers[k].elementID + item).closest('label').css('color', 'green');
                                $('#Quest_choice' + $answers[k].elementID + item).closest('label').css('font-weight', 'bold');
                            }
                        }
                    }
                }
            });    
    }


   
    zegar();


    $('#SelectedCategory1').change(function () {
        var htmlSubCategory = $('#SelectedSubCategory');
        var selectedCategoryItem = $(this).val();
        $.ajax({
            dataType: "JSON",
            type: "GET",
            url: "/Editor/GetSubCategoryByCategoryId",
            data: { 'categoryId': selectedCategoryItem },
            success: function (data) {
                htmlSubCategory.html('');
                var district = "<select id='SelectedSubCategory'>";
                district = district + '<option value="">--- wybierz podkategorię ---</option>';
                for (var i = 0; i < data.length; i++) {
                    district = district + '<option value=' + data[i].value + '>' + data[i].text + '</option>';
                }
                district = district + '</select>';
                htmlSubCategory.html(district);
            },
        });
    });



    
});

function proba() {
    
    var z = 1;
    console.log(z);
}


var  spinnerVisible = false;
    function showProgress() {
        if (!spinnerVisible) {
        $("div#spinner").fadeIn("fast");
    spinnerVisible = true;
        }
};

    function hideProgress() {
        if (spinnerVisible) {
            var spinner = $("div#spinner");
            spinner.stop();
            spinner.fadeOut("fast");
            spinnerVisible = false;
        }
    };


function zegar() {


        var miesiace = ['stycznia', 'lutego', 'marca', 'kwietnia', 'maja', 'czerwca',
         'lipca', 'sierpnia', 'września', 'października', 'listopada', 'grudnia'],
            dniTygonia = ['niedziela', 'poniedziałek', 'wtorek', 'środa', 'czwartek', 'piątek', 'sobota'],
            data = new Date(),
            nazwaMiesiaca = miesiace[data.getMonth()],
            nazwaDnia = dniTygonia[data.getDay()]
          
   


   
    var day = data.getDate();
    var month = data.getMonth()+1;
    var year = data.getFullYear();
    var godzina = data.getHours();
    var min = data.getMinutes();
    var sek = data.getSeconds();
    var terazjest = day + " " + nazwaMiesiaca + " " + year + " " + godzina +
        ((min < 10) ? ":0" : ":") + min +
        ((sek < 10) ? ":0" : ":") + sek; 
    $("#watch").html(terazjest);

    setTimeout("zegar()", 1000);
}

function updateTime() {
    var d = new Date(),
        h = d.getHours(),
        m = d.getMinutes();
    if (h < 10) h = "0" + h;
    if (m < 10) m = "0" + m;
    $('#time').html(h + ":" + m);
    $('#date').html(stringifyDate());

    if (d.getMonth() === 9) {
        $('#date').addClass("small-font");
    } else {
        $('#date').removeClass("small-font");
    }
}


    // when the modal is closed
    $('#modal-container').on('hidden.bs.modal', function () {
        // remove the bs.modal data attribute from it
        $(this).removeData('bs.modal');
        // and empty the modal-content element
        $('#modal-container .modal-content').empty();
});

$('#modal-container').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var url = button.attr("href");
    var modal = $(this);

    // note that this will replace the content of modal-content ever time the modal is opened
    modal.find('.modal-content').load(url);
});

