﻿
$(document).ready(function () {

    var areaName = $('#Area').text();
    $('#SelectedCategory').change(function () {      
        var htmlSubCategory = $('#SelectedSubCategory');
        var selectedCategoryID = $(this).val();
        $.ajax(
            {
                dataType: 'JSON',
                type    : 'GET',
                url: '/' + areaName + '/Edit/GetSubCategoryByCategoryId',
                data: { 'categoryID': selectedCategoryID },
                success: function (data) {
                    htmlSubCategory.html('');
                    var district = "<select id='SelectedSubCategory'>";
                    district = district + '<option value="">--- wybierz podkategorię ---</option>';
                    for (var i = 0; i < data.length; i++)
                    {
                        district = district + '<option value=' + data[i].value + '>' + data[i].text + '</option>';
                    }
                    district = district + '</select>';
                    htmlSubCategory.html(district);
                }
            });
    });
});