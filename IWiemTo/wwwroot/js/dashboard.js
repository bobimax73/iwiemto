﻿//zmienna globalna, google tak pisze ze musi byc - nie wnikam
google.load("visualization", "1", { packages: ["corechart"] });  


$(document).ready(function () {

    var areaName = $('#Area').text();
    var userID = $('#UserID').text();
    var dateRange = 1; // cały zakres
    var quizID = 1;
    // tabela wyświetla pytania Quizu
    

    showProcentResults(0,null,null);
    showActivities();

    $(".procentResult #SetDateTime0, #SetDateTime3, #SetDateTime1").click(function () {
        // wybór jaka Data 0 - wszyskie wyniki (domyślnie) 
        //                 1 - za dany miesiac 
        //                 2 - za dany dzień 
        //                 3 - dziś
        var dateRange = $(this).data().daterange; 
        showProcentResults(dateRange,null,null); 
    });

    $("#SetDateTime2").click(function () {     
        $('#detailModalChoseDate').modal('show'); 
       
    });
    
  
   
    $('#detailModalChoseDate').on('click', '#paramsShow', function (e) {
       
        var dateFrom = $('#dateFrom').val();
        var dateTo = $('#dateTo').val();       
        showProcentResults(2, dateFrom, dateTo);        
        $('#detailModalChoseDate').modal('hide');
    });


    $(".top5Results").show(function () {
        $.ajax({
            type: 'GET',
            url: '/' + areaName + '/' +'Dashboard/Top5Results',
            success: function (response) {
                $(".top5Results").html(response);
            },
            error: function () {
                alert("Bład w trkacie ładowania danych!");
            }
        });
    });


    $(".top5Questions").show(function () {
        $.ajax({
            type: 'GET',
            url: '/' + areaName + '/' +'Dashboard/Top5Questions',
            success: function (response) {
                $(".top5Questions").html(response);
            },
            error: function () {
                alert("Bład w trkacie ładowania danych!");
            }
        });
    });

    $(".top5SubCategory").show(function () {
        $.ajax({
            type: 'GET',
            url: '/' + areaName + '/' +'Dashboard/Top5SubCategories',
            success: function (response) {
                $(".top5SubCategory").html(response);
            },
            error: function () {
                alert("Bład w trkacie ładowania danych!");
            }
        });
    });

    quizDataTable= $(".panel #quizDataTable").DataTable( {

        
        "oLanguage": { sProcessing: "<img id='sProcessing' src='/images/ajax-loader.gif'>", sUrl: "/lib/DataTables/Polish.json" },
        "processing": true, // for show progress bar  
        "serverSide": true, // for process server side
        "select"    : true,     
        "filter"    : true, // this is for disable filter (search box)  
        "orderMulti": false, // for disable multiple column at once 
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "ajax":
        {
              "url": '/' + areaName + '/' +"Dashboard/AllQuizDataTable",
             "type": "POST",
            "datatype": "json",
            data: { 'userID': userID }
        },
        "columnDefs":
            [
              
                { "targets": 1, "width": "10%", "visible": false, "searchable": false },                
                { "targets": 2, "width": "20%", "render": function (data) { return moment(data).format('DD-MM-YYYY HH:mm') } },
                { "targets": 3, "width": "50%", "visible": true, "searchable": false },
                { "targets": 4, "width": "15%", "visible": true, "searchable": false },
                { "targets": 5, "width": "30%", "visible": true, "searchable": false },
                { "targets": 6, "width": "15%", "visible": false, "searchable": false },
                { "targets": 7, "width": "8%", "visible": true, "searchable": false }

            ],
        "columns":
            [
                {
                    "data": "procent", "render": function (data, type, row, meta) {
                        if (type == "display") {
                            if (data >= 60) {
                               
                                icon = '<i class="fa fa-smile-o fa-2x" style="color:green"></i>';
                            }
                            else
                                icon = '<i class="fa fa-frown-o fa-2x" style="color:gray"></i>';
                            return icon;
                        }
                        else {

                            return data;
                        }

                    }
                    
                },
               
                { "data": "quizID", "autoWidth": true },
                { "data": "quizCreateDate", "autoWidth": true },
                { "data": "quizDescribe"           , "autoWidth" : true },        
                { "data": "quizModeName", "autoWidth": true },   
                { "data": "procent", "autoWidth": true },     
                { "data": null, "render": function (data, type, full, meta) { return full["userScore"] + "/" + full["quizAllScore"] + ".pkt"; } },
                { "className": "details-control", "data": null, "defaultContent": '', "render": function (data, type, full, meta) { return '<i class="fa fa-arrow-circle-down fa-1.5x"></i>' } }
        ]
    });


    function format(d) {
       
        return '<span style="color:blue">Szczegóły wyniku quizu: </span>' + '<span>' + d.quizName + '</span>' +
            '<hr/>' +
            '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
            '<tr>' +
            '<td>Liczba pytań quizu:</td>' +
            '<td style="padding-left:5px;">' + d.quizQuestionCount + '</td>' +
            '<td style="padding-left:20px;">Punktacja: </td>' +
            '<td style="padding-left:20px;">' + d.userScore + '/' + d.quizAllScore + ' pkt.' + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Pytania zaliczone:</td>' +
            '<td style="padding-left:5px;">' + d.questionsCountPositive + '</td>' +
            '<td style="padding-left:20px;">Procent: </td>' +
            '<td style="padding-left:20px;">' + d.procent + '%' + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Pytania niezaliczone:</td>' +
            '<td style="padding-left:5px;">' + d.questionsCountNegative +'</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Tutaj znajdziesz <a href="#"  class="QuizDetail" style="font-weight:bold" data-quizID= '+d.quizID+'>szczegóły</a> pytań</td>' +   
            '</tr>'+
            '</table>' +
            '<hr/>';
    }

    $('.panel #quizDataTable tbody').on('click', 'tr', function () {
        console.log(quizDataTable.row(this).data());
    });


    $('.panel #quizDataTable tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var table = $('.panel #quizDataTable');
        var row = quizDataTable.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child(format(row.data())).show();
            tr.addClass('shown');
        }
    });


    //Pobiera z bazy i wyświetla w okno Modalnym "detailModaleg" 
    //szczegółowe dane o pytaniu
    $('.panel').on('click', '.pull-left-DetailQuestion', function () {
        var questionID = $(this).data('questionid');
        $.ajax({
            type: 'GET',
            url: '/' + areaName + '/' +'Dashboard/ShowQuestion',
            data: { 'questionID' : questionID },
          
            success: function (response) {
                $('.modal-body').html(response);
                $('#detailModal').modal('show'); 
            },
            error: function () {
                alert("Bład w trkacie ładowania danych!");
            }

        })
        
    });

    function showProcentResults(dateRange,dataFrom,dataTo) {        
        var dateRangeText = "Rozkład wyników uzyskanych przez ucznia w całym okresie";
        if (dateRange == 3) { dateRangeText = "Rozkład wyników uzyskanych przez ucznia w dniu dzisiejszym"; }
        if (dateRange == 2) { dateRangeText = "Rozkład wyników uzyskanych przez ucznia w okresie od: " + dataFrom +" do: " +dataTo }
        if (dateRange == 1) { dateRangeText = "Rozkład wyników uzyskanych przez ucznia w bieżącym miesiącu"; }
        $.ajax({
            type: 'GET',
            url: '/' + areaName + '/' +'Dashboard/ProcentResults',
            dataType: "JSON",
            data: { 'dateRange': dateRange, 'dataFrom': dataFrom, 'dataTo': dataTo },
            success: function (response) {
                console.log(response);
                hideProgress();
                var data = new google.visualization.DataTable();

                data.addColumn('string', 'RangeName');
                data.addColumn('number', 'ProcentResultCount');


                for (var i = 0; i < response.result.length; i++) {
                    data.addRow([response.result[i].rangeName, response.result[i].procentResultCount]);
                }

               
                var chart = new google.visualization.PieChart(document.getElementById('orderProcentResults'));

                chart.draw(data,
                    {
                        title: dateRangeText ,
                        position: "bottom",
                        fontsize: "10px",
                        chartArea: { width: 600, 'height': 165},
                        is3D: true,
                        interpolateNulls: true,
                        sliceVisibilityThreshold: 0
                    });
            },
            error: function () {
            
                alert("Bład ładowania danych!");
            }
        });

    }  


    function showActivities() {
        $.ajax({
            type: 'GET',
            url: '/' + areaName + '/' +'Dashboard/Activities',
            dataType: "JSON",
            success: function (response) {
                
                var data = new google.visualization.DataTable();

                data.addColumn('date'  , 'QuizCreateDate');
                data.addColumn('number', 'Liczba quizów'     );   
             
                for (var k = 0; k <= 6; k++)
                {
                    var currDate = new Date();
                    currDate.setDate(currDate.getDate() - k);    
                    currDate.setHours(0, 0, 0, 0);
                    var count = 0;
                    for (var i = 0; i < response.result.length; i++) {

                        var date = new Date(response.result[i].quizCreateDate);


                        var dataQuiz = new Date(response.result[i].quizCreateDate);
                        var year = dataQuiz.getFullYear();
                        var month = dataQuiz.getMonth();
                        var day = dataQuiz.getDate();
                        dataQuiz = new Date(year, month, day);
                        
                        if (dataQuiz.getTime() == currDate.getTime())  {
                            count = response.result[i].quizCount;
                        }
                    }
                                                               
                    data.addRow([currDate, count]);

                }


              /*  for (var i = 0; i < response.result.length; i++) {
                  
                    var dataQuiz = new Date(response.result[i].quizCreateDate);
                    var year  = dataQuiz.getFullYear();
                    var month = dataQuiz.getMonth();
                    var day = dataQuiz.getDate();

                    console.log(new Date(year,month,day));
                    data.addRow([new Date(year, month, day), response.result[i].quizCount]);
                }*/





                var chart = new google.visualization.ColumnChart(document.getElementById('quizActivity'));

                chart.draw(data,
                    {
                        title: "Liczba wykonanych quizów w okresie czasowym",
                        position: "top",
                        fontsize: "16px",
                       
                        hAxis: {
                            format: 'dd-MM',
                            gridlines: { count: 15 },
                            title: 'Dzień'
                        },
                        vAxis: {
                            gridlines: { color: 'lightgray' },
                            minValue: 0, 
                            title : 'Liczba quizów'
                        }
                   
                    });
            },
            error: function () {

                alert("Bład ładowania danych!");
            }
        });

    }

    // kliknięcie w link - Tutaj znajdziesz szczegóły quizu/pytań

    $('body').on('click', 'a.QuizDetail', function () {

        $('#ModalWindowQuizDetail').modal('toggle');
        var quizID = $(this).data('quizid');
         var table = $('#datatableQuizDetail').DataTable({
                "destroy": true,
                "retrive": true,
                "oLanguage": { sProcessing: "<img id='sProcessing' src='/images/ajax-loader.gif'>", sUrl: "/lib/DataTables/Polish.json" },
                "processing": true, // for show progress bar  
                "serverSide": true, // for process server side
                "filter": true, // this is for disable filter (search box)  
                "orderMulti": false, // for disable multiple column at once 
                "lengthMenu": [[1, 10, 25, 50, -1], [1, 10, 25, 50, "All"]],
                "responsive": true,
                 "ajax":

                {
                    "url": '/' + areaName + '/' + 'Dashboard/ShowAllQuestionsByQuizID',
                    "type": 'POST',
                    "dataType": 'JSON',
                    "data": { 'quizID': quizID }
             },
             
                "columns":
                    [
                       
                        { data: "questionText", render: function (data, type, row) { return Question(data,type,row);} }
                       
                    ]
            });

    });

    function Question(data, type, row) {



        if (row.isCorect == true) {
            status_text = '<div class="panel-heading" style="background-color:lightgreen">';
            status_ico = '<i class="fa fa-smile-o fa-2x" style="color:white"></i>';
        }
        else
        {
            status_text = '<div class="panel-heading" style="background-color:red">';
            status_ico  = '<i class="fa fa-frown-o fa-2x" style="color:white"></i>';
        }



        var result =
            
            '<div class="panel panel-default" >' +
            status_text +  status_ico + 
            '<i  style="margin-left:24px;"></i><strong>Pytanie Q </strong>' + row.questionID +'</br>'+'</hr>'+
            '<i  style="margin-left:50px;"></i><strong>Punktacja   : </strong>' + row.score + '/' + row.questionPoints + '</br>' +
            '<i  style="margin-left:50px;"></i><strong>Kategoria   : </strong>' + row.categoryName + ' → ' + row.subCategoryName + ' </br>' +
            '<i  style="margin-left:50px;"></i><strong>Rodzaj  : </strong>' + row.typeName + ' </br>' +
            '</div>' +           
              '<div class="panel-body Question">' +
                '<div class="BlockQ" style="border: 1.5px solid #bdbdbd;   width: 90%; border-radius: 4px; margin-top: 5px; margin-left:10px; background-color:#f0ffff; padding: 8px;">' +
            data + '</br></br>' + addImage(data, type, row) +             
            
            '</div>' +
            addSubQuestion(data, type, row) +
             '</div>' +
            '</div>';

     
        return result;
    }
   
    function addImage(data, type, row) {
        var imageHtml = '';
        if (row.questionsImageRel1[0].image != null) {
            var imagePath = '/images/Question/Image/'+ row.questionsImageRel1[0].image;
            imageHtml = '<div class="Image" style="margin-left:3%; background-color:#f0ffff">' + '<img style="background-color:none;" src=' + '"' + imagePath + '"' + '/>' + '</div>';        
        }
        return imageHtml;
    }


    function addSubQuestion(data, type, row) {

        var subQuestionHtml = '';
        var nr = 1;
        for (var k = 0; k < row.questionsChooseRel.length; k++)
        {
            if (row.typeID == 1)   // zapytanie jeden z wielu
            {
                subQuestionHtml = subQuestionHtml +
                    '<div class="col-md-10 col-md-offset-1 colSubQuestion" style="margin-top:10px">' +
                    '<label class="radio-inline">' +
                        '<input type="radio" name=' + '"' + 'Q_' + row.questionID + "'" + ' id = ' + '"' + 'Q_' + row.questionID + k + '"' + ' value =' + '"' + row.questionsChooseRel[k].choiceText +'"' + ' />' + row.questionsChooseRel[k].choiceText +
                    '</label >' +
                    '</div >';
            }

            if (row.typeID == 2) // pytanie wiele z wielu
            {
                subQuestionHtml = subQuestionHtml +
                    '<div class="col-md-10 col-md-offset-1 colSubQuestion" style="margin-top:10px">' +
                    '<label class="radio-inline">' +
                    '<input type="checkbox" name="' + 'Q_' + row.questionID  + '"' + ' value="' + row.questionsChooseRel[k].choiceText + '"' + '/>' + row.questionsChooseRel[k].choiceText +
                    '</label >' +
                    '</div >';
            }

            if (row.typeID == 3 || row.typeID == 4 ) // pytanie wiele z wielu
            {
                subQuestionHtml = subQuestionHtml +
                    '<div class="col-md-10 col-md-offset-1 colSubQuestion" style="margin-top:10px">' +
                    '<div class="form-group purple-border" id="' + 'Div_textarea_' + row.questionID + '">' +' Q' +  nr+'. ' +
                    '<label style="font-size:1.5rem;font: normal  Tahoma, Geneva, sans-serif" for="exampleFormControlTextarea4">' + '</label>' +
                    row.questionsChooseRel[k].choiceText +
                    '</div>' +
                    '</div>';
            }
            if ( row.typeID == 5 ) {

                subQuestionHtml = subQuestionHtml +
                    '<div class="col-md-10 col-md-offset-1 colSubQuestion" style="margin-top:10px">' +
                    '<div class="form-group purple-border" id="' + 'Div_textarea_' + row.questionID + '">' + 'Q' + nr + '. ' +
                    '<label>' + '</label>' + row.questionsChooseRel[k].choiceText + ' (Prawda|Fałsz) '+                       
                    '</div>'+
                    '</div>';
            }

            nr ++;
        }

        return subQuestionHtml;
    }


    // obsluga DataTable  -> "tableComments" z DashBoard    
    //        datasource  ->  kontroler /areaName/Dashborad
    // dodane 21.11.2019


    $('.panel #tableComments').DataTable(
        {
            "oLanguage" : { sProcessing: "<img id='sProcessing' src='/images/ajax-loader.gif'>", sUrl: "/lib/DataTables/Polish.json" },
            "processing": true,  
            "serverSide": true  ,          
            "ajax":
           
            {
                "url": '/' + areaName + '/' + "Dashboard/ShowAllQuestionsByQuizID",   
                "type"    : 'POST',
                "datatype": 'JSON',
                "data"    : { 'userID': userID }
            },
            "columns":
                [
                    { data: "dateCreate", render: function (d) { return moment(d).format("DD-MM-YYYY HH:mm"); }, width: '30%' },
                    { data: "commentsType", title: "Rodzaj zgłoszenia" },
                    { data: "comments" },
                    {
                        data: "status", width: '10%', render: function (data, type, row, meta) {
                            if (type === "display" || type === "filter")
                            {
                                
                                switch ( data) {                                    
                                    case 1:
                                        return '<i title="zgłoszenie zostało zaakceptowane" data-toggle="tooltip" data-placement="top" class="fas fa-thumbs-up" style="color:green"></i>';                                        
                                         // komentarz uznany
                                    case 2:
                                        return '<i title="zgłoszneie zostało odrzucone" data-toggle="tooltip" data-placement="top" class="fas fa-thumbs-down" style="color:red"></i>';
                                        // komentarz odrzucony
                                    default: 
                                        return '<i title="w trakcie analizy..." data-toggle="tooltip" data-placement="top" class="fas fa-cart-plus fa-1.7x" style="color:balck"></i>';
                                       // komentarz w trakcie analizy
                                }

 
                            }
                            else return data;
                            
                        }
                    }
            ]
        }

    );



});