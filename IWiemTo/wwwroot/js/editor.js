﻿
$(document).ready(function () {

    var areaName = $('#Area').text();

    $.ajax({
        type: "GET",
        url: "/"+areaName +"/"+"Editor/GetDictQuestionType",
        data: "{}",
        success: function (data) {
            s = '';
            for (var i = 0; i < data.length; i++)
            {                               
                $('#questionTypeDropdown').append('<li value=' + data[i].typeID + '>' + '<a href="#">' + data[i].typeName + '</a></li>');
            }           
            $('#questionTypeDropdown li').on('click', function () {
                var questionTypeName = '', url = '', questionTypeID = 0; pageName = '';
                questionTypeName = $(this).text();
                questionTypeID = $(this).attr('value'); 
                pageName = (questionTypeID == 1) || (questionTypeID == 2) ? pageName = "CreateQuestClose" : pageName = "CreateQuestOpen";
                url = "/" + areaName + "/" + "Editor/Create?pageName=" + pageName + "&questionTypeName=" + questionTypeName + "&questionTypeID=" + questionTypeID + "&areaName=" + areaName;               
                window.location = url;
            });
        }
    });  
    $('#btnCreate').click(function () {

        window.location = "/" + areaName + "/" +"/Editor/Create?pageName=" + "Create";   
        //$.get('@Url.Action("Create","Editor")');
        //document.location = '@Url.Action("Create","Editor")';
    });

    $('#checkbox-value').text($('#checkbox0').val());

    $("#checkbox0").on('change', function () {
        if ($(this).is(':checked')) {
            $(this).attr('value', 'true');
        } else {
            $(this).attr('value', 'false');
        }

        $('#checkbox-value').text($('#checkbox0').val());
    });

    $("#checkbox1").on('change', function () {
        if ($(this).is(':checked')) {
            $(this).attr('value', 'true');
        } else {
            $(this).attr('value', 'false');
        }

        $('#checkbox-value').text($('#checkbox1').val());
    });

    $("#checkbox2").on('change', function () {
        if ($(this).is(':checked')) {
            $(this).attr('value', 'true');
        } else {
            $(this).attr('value', 'false');
        }

        $('#checkbox-value').text($('#checkbox2').val());
    });


    $("#checkbox3").on('change', function () {
        if ($(this).is(':checked')) {
            $(this).attr('value', 'true');
        } else {
            $(this).attr('value', 'false');
        }

        $('#checkbox-value').text($('#checkbox3').val());
    });

    $("checkbox4").on('change', function () {
        if ($(this).is(':checked')) {
            $(this).attr('value', 'true');
        } else {
            $(this).attr('value', 'false');
        }

        $('#checkbox-value').text($('#checkbox4').val());

    });

    $('#TableQuestions').DataTable(
        {
            "oLanguage": {
                sProcessing: "<img id='sProcessing' src='/images/ajax-loader.gif'>",
                sUrl: "/lib/DataTables/Polish.json"
            },
            "processing": true, // for show progress bar  
            "serverSide": true, // for process server side
            "select"    : true,            
            "filter"    : true, // this is for disable filter (search box)  
            "orderMulti": false, // for disable multiple column at once 
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],

            "ajax": {
                "url": "/" + areaName  +"/" +"Editor/GetAllQuestionFromDB",
                "type": "POST",
                "datatype": "json"

            },
            "columnDefs":
                [{
                    "targets": [0],
                    "visible": true,
                    "searchable": false
                }],
            "columns":
                [
                    { name:"QuestionID"            ,data: "questionID"           , title: "ID"          , "autoWidth": true },
                    { name:"QuestionText"          ,data: "questionText"         , title: "Treść"       , width: '100%' },
                    { name: "QuestionShortDescribe",data: "questionShortDescribe", title: "Opis"        , "autoWidth": true},
                    { name:"QuestionTypeContent"   ,data: "questionTypeContent"  , title: "Typ"         , "autoWidth": true },
                    { name:"CategoryName"          ,data: "categoryName"         , title: "Kategoria"   , "autoWidth": true },
                    { name:"SubCategoryName"       ,data: "subCategoryName"      , title: "Podkategoria", "autoWidth": true },
                    { name:"QuestionPoints"        ,data: "questionPoints"       , title: "Punkty"      , "autoWidth": false },
                    { name:"QuestionSource"        ,data: "questionSource"       , title: "Źródło"      , "autoWidth": true },
                    { name:"TypeName"              ,data: "typeName", title: "Rodzaj pytania", "autoWidth": true },
                    { name: 'EditQuest', title: 'Edycja', render: function (data, type, full)
                        { return '<a class="btn btn-primary" href=/' + areaName + '/Edit/Index?questionID=' + full.questionID + '>Pytania</a> <a style="margin-top:5px" class="btn btn-info" href=/' + areaName + '/Edit/Index?questionID=' + full.questionID + '>Odpowiedzi</a>'; }
                    }
                    
                ]

        });

    $('#SelectedCategory').change(function () {
        var htmlSubCategory = $('#SelectedSubCategory');
        var selectedCategoryItem = $(this).val();
        $.ajax({
            dataType: "JSON",
            type: "GET",
            url: "/" + areaName+ "/" +"Editor/GetSubCategoryByCategoryId",
            data: { 'categoryId': selectedCategoryItem },
            success: function (data) {
                htmlSubCategory.html('');
                var district = "<select id='SelectedSubCategory'>";
                district = district + '<option value="">--- wybierz podkategorię ---</option>';
                for (var i = 0; i < data.length; i++) {
                    district = district + '<option value=' + data[i].value + '>' + data[i].text + '</option>';
                }
                district = district + '</select>';
                htmlSubCategory.html(district);
            },
        });
    });
    


});