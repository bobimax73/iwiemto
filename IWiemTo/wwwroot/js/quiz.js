﻿$(document).ready(function () {

    SetField_LevelKnowledgeName();
    SetQuizName();   

   

    $('#QuizModeID').change(function () {
        SetQuizName();

        var htmlQuizCount = $('#quizMaxQuestionCount');
        var selectedMode = $("#QuizModeID option:selected").text();
        var htmlQuestionMode = $('#QuestionsModeID');
        var htmlCategoryID = $('#CategoryID');
        var htmlQuestionNumbers = $('#QuestionNumbers');
       
        if (selectedMode == 'nauka') {

            htmlQuizCount.val(3);
            htmlQuizCount.attr('max', 15);
            htmlQuestionMode.prop('disabled', false);
            htmlCategoryID.prop('disabled', false);
            htmlQuestionNumbers.prop('disabled', false);
            htmlQuestionNumbers.attr('placeholder','wprowadź numery pytań dla quizu np. 1,21,121 lub fragment treści pytania np. serce');

        }
        if (selectedMode == 'sprawdzian') {

            htmlQuizCount.val(10);
            htmlQuizCount.attr('max', 30);
            htmlQuestionMode.val($("#QuestionsModeID option:eq(0)").val());
            htmlCategoryID.val($("#CategoryID option:eq(0)").val());
            htmlQuestionMode.prop('disabled', true);
            htmlCategoryID.prop('disabled', true);
            htmlQuestionNumbers.prop('disabled', true);
            htmlQuestionNumbers.val('');
            htmlQuestionNumbers.attr('placeholder', '');

        }
    });

    $('#LevelKnowledgeID').change(function () {

        var LevelKnowledgeIDselectedValue = $('#LevelKnowledgeID option:selected').text();
        $('#LevelKnowledgeName').val(LevelKnowledgeIDselectedValue);
    }
    );

    $('#datetimepicker1').on('dp.change', function (e) {
        SetQuizName();
    });

    $('#datetimepicker1').datetimepicker({
        format: 'YYYY-MM-DD HH:mm',
        useCurrent: true,
        defaultDate: new Date()
    });
   

    $('#SubCategoryID').multiselect({ 
       
        buttonWidth: "100%",       
        selectAllText: ' zaznacz wszystko',
        nonSelectedText:        'wybierz temat quizu ',
        includeSelectAllOption: 'false',
        numberDisplayed: 10,
        templates: { button: '<button type="button"  id="mutiselectButton"class="multiselect dropdown-toggle" data-toggle="dropdown"><span class="multiselect-selected-text"></span> <b class="caret"></b></button>' }
    });

    function SetField_LevelKnowledgeName() {
        var selectedText = $('#LevelKnowledgeID option:selected').text();
        $('#LevelKnowledgeName').val(selectedText);
    }

    function SetQuizName() {
        var selectedUser = $("#UserName").text();
        var selectedMode = $("#QuizModeID option:selected").text();
        var htmlDateCurr = $("#datetimepicker1").find("input").val();
        var htmlQuizName = $('#quizName');
        var htmlQuizModeName = $('#quizModeName');
        
        htmlQuizName.html('');
        htmlQuizName.val('quiz' + '_' + selectedUser + '_' + selectedMode + '_' + htmlDateCurr);
        htmlQuizModeName.val(selectedMode);
    }

   


    
});


 
