﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;
using IWiemTo.Data;


namespace IWiemTo
{
    public class QuestionsEditor : IQuestionsEditor, IDisposable
    {

        private bool _disposed = false;
       

        public QuestionsEditor()
        {
           
        }
    
       public  IEnumerable<SelectListItem> LoadTypeOfQuestionContent(AppDBContext _appDbContext)
        {
            var listTypeQuestionContent = new List<SelectListItem>();

            var item = new SelectListItem() { Value= "0", Text = "zakres podstawowy" };
            listTypeQuestionContent.Insert(0, item);

            item = new SelectListItem()   { Value = "1", Text = "zakres rozszerzony" };
            listTypeQuestionContent.Insert(1, item);

            return listTypeQuestionContent;
        }


        public IEnumerable<SelectListItem> GetCategories(AppDBContext _appDbContext)
        {
            List<SelectListItem> category = _appDbContext.Dict_Category.AsNoTracking().OrderBy(n => n.CategoryName).Select(
                                             n => new SelectListItem { Value = n.CategoryID.ToString(), Text = n.CategoryName })
                                             .ToList();
            var tip = new SelectListItem() { Value = null, Text = "--- wybierz kategorie ---" };
            category.Insert(0, tip);
            return category;
        }


        public IEnumerable<SelectListItem> GetSubCategories(AppDBContext _appDbContext)
        {

            List<SelectListItem> subCategory = _appDbContext.Dict_SubCategory.AsNoTracking().OrderBy(n => n.SubCategoryName).Select(
                                n => new SelectListItem { Value = n.SubCategoryID.ToString(), Text = n.SubCategoryName })
                                .ToList();

            var tip = new SelectListItem() { Value = null, Text = "--- wybierz podkategorię ---" };
            subCategory.Insert(0, tip);
            return subCategory;
        }


        public IEnumerable<SelectListItem> GetSubCategoriesByCategoryID(AppDBContext _appDbContext, int? categoryID)
        {

            List<SelectListItem> subCategory = _appDbContext.Dict_SubCategory.Where(item=>item.CategoryID==categoryID).AsNoTracking().OrderBy(n => n.SubCategoryName).Select(
                                n => new SelectListItem { Value = n.SubCategoryID.ToString(), Text = n.SubCategoryName })
                                .ToList();          
            return subCategory;
        }



        public IEnumerable<SelectListItem> GetLevelKnowledge(AppDBContext _appDbContext)
        {
            List<SelectListItem> category = _appDbContext.LevelKnowledgeTable.AsNoTracking().OrderBy(n => n.LevelKnowledgeID).
                Select( n => new SelectListItem { Value = n.LevelKnowledgeID.ToString(), Text = n.LevelKnowledgeName, Selected = n.LevelKnowledgeID==2?true : false }).ToList();            
            return category;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                   
                }
            }
            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
