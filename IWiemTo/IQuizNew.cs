﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using IWiemTo.Data;


namespace IWiemTo
{
    public interface IQuizNew : IDisposable
    {

        //  pobranie ze słownika trybu nauki
        IEnumerable<SelectListItem> Load_DictQuizMode( AppDBContext _appDbContext );

        // pobranie ze słownika Question Type
        List<SelectListItem> Load_DictQuestionType( AppDBContext _appDbContext );

        // pobranie słownika Podkategorie 
        List<SelectListItem> Load_DictCategoryView( AppDBContext _appDbContext );

        // pobranie słownika Poziomu wiedzy - szkoła podstawowa szkoła średnia
        List<SelectListItem> Load_DictLavelKnowledge(AppDBContext _appDbContext);
    }
}

