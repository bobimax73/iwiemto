﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Rendering;
using IWiemTo.Data;
using IWiemTo.Models;
using System.Reflection;


namespace IWiemTo.Areas
{
    [Authorize]
    [Area("Biologia")]

    public class EditorController : Controller
    {
        private DbBiolContext _appDBContext;        
        private IQuestionsEditor _repo;
        private readonly UserManager<ApplicationUser> _userManager;
        private List<QuestionsView> quest;

        public EditorController(DbBiolContext  context, UserManager<ApplicationUser> userManager, IQuestionsEditor questEdytor)
        {
            _appDBContext = context;
            _repo = questEdytor;
            _userManager = userManager;
        }


        public IActionResult Index()
        {           
            ViewBag.areaName = typeof(EditorController).GetCustomAttribute<AreaAttribute>().RouteValue;
            return View();
        }

        [HttpPost]
        public IActionResult GetAllQuestionFromDB()
        {
           
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            // Skiping number of Rows count  
            var start = Request.Form["start"].FirstOrDefault();
            // Paging Length 10,20  
            var length = Request.Form["length"].FirstOrDefault();
            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int recordsTotal = 0;
            

            // Sort Column Name  
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDir = Request.Form["order[0][dir]"].FirstOrDefault();

            //Sorting    
            if (!(string.IsNullOrEmpty(sortColumn)) && (!string.IsNullOrEmpty(sortColumnDir))) 
            {
                var propertyInfo = typeof(QuestionsView).GetProperty(sortColumn);
                if (sortColumnDir=="asc")
                    quest = _appDBContext.QuestionsView.OrderBy(x =>propertyInfo.GetValue(x,null)).ToList();
                if (sortColumnDir == "desc")
                    quest = _appDBContext.QuestionsView.OrderByDescending(x => propertyInfo.GetValue(x, null)).ToList();
            }


            // Search Value from (Search box)  
            var searchValue = Request.Form["search[value]"].FirstOrDefault();
            if (!string.IsNullOrEmpty(searchValue))
            {
                var searchQuery = quest.Where(k => k.QuestionID.ToString() == searchValue ||
                    (k.QuestionShortDescribe!=null && k.QuestionShortDescribe.Contains(searchValue) ) || k.QuestionText.Contains(searchValue)||k.QuestionSource.Contains(searchValue)|| k.SubCategoryName.Contains(searchValue)).ToList();
                quest = searchQuery;
            }
            
            recordsTotal = quest.Count();

            var data = quest.Skip(skip).Take(pageSize).ToList();
            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
        }



        //akcja zwraca słownik typow pytań
        public JsonResult GetDictQuestionType()
        {
            var table = _appDBContext.Dict_QuestionsType.ToList();
            return Json(table);
        }


        public JsonResult GetSubCategoryByCategoryId(string categoryId)
        {
            if (String.IsNullOrEmpty(categoryId))
            {
                throw new ArgumentNullException("categoryId");
            }
            int id = 0;
            bool isValid = Int32.TryParse(categoryId, out id);
            var subCategory = _appDBContext.Dict_SubCategory.AsNoTracking().Where(n => n.CategoryID == id).OrderBy(n => n.SubCategoryName).Select(
                n => new SelectListItem { Value = n.SubCategoryID.ToString(), Text = n.SubCategoryName }).ToList();
            return Json(subCategory);
        }


        //GET Create new question /Editor/Create
        [HttpGet]
        public IActionResult Create(string pageName, string questionTypeName, int questionTypeID, string areaName )
        {                
            ViewData[ "questionType"  ]        =  questionTypeName;
            ViewData[ "questionTypeID"]        =  questionTypeID;
            ViewData[ "Category"      ]        = _repo.GetCategories(_appDBContext);
            ViewData[ "SubCategory"   ]        = _repo.GetSubCategories(_appDBContext);
            ViewData[ "TypeContentOfQuestion"] = _repo.LoadTypeOfQuestionContent(_appDBContext);
            ViewBag.LevelKnowledge             = _repo.GetLevelKnowledge(_appDBContext);
            ViewBag.AreaName                   = typeof(EditorController).GetCustomAttribute<AreaAttribute>().RouteValue;
            return View(pageName);
        }

           
        [HttpPost]
        public ActionResult NewQuestion(Models.Questions  quest)
        {
            
            if (ModelState.IsValid)
            {
                quest.QuestionCreated = DateTime.Now;
                quest.QuestionsChoiceRel.RemoveAll(v => v.ChoiceText == null || string.IsNullOrEmpty(v.ChoiceText));
                quest.QuestionCategory.Add(new Models.QuestionCategory {CategorySubID = quest.SelectedSubCategory, CategoryID = quest.SelectedCategory });                
                _appDBContext.Questions.Add(quest);
                _appDBContext.SaveChanges();               
                return RedirectToAction("Index");
            }

            return View("Index");
        }

        [HttpPost]
        public ActionResult NewOpenQuestion(Models.Questions quest, List<Models.Answers> answer)
        {
            if (ModelState.IsValid)
            {

                quest.QuestionsChoiceRel.RemoveAll(v => v.ChoiceText == null || string.IsNullOrEmpty(v.ChoiceText));
                quest.QuestionCategory.Add(new Models.QuestionCategory { CategorySubID = quest.SelectedSubCategory, CategoryID = quest.SelectedCategory });
                answer.RemoveAll(v => v.Answer == null || string.IsNullOrEmpty(v.Answer));
                _appDBContext.Questions.Add(quest);                
                 _appDBContext.SaveChanges();

                int k = 0;
                foreach (var item in quest.QuestionsChoiceRel)
                {
                    answer[k].QuestionChoiceID = item.QuestionChoiceID;
                    answer[k].QuestionID = item.QuestionID;
                    _appDBContext.Answers.Add(answer[k]);
                    k++;
                }
                _appDBContext.SaveChanges();
                return RedirectToAction("Index");
         

            }

            return View("Index");
        }


    }
}