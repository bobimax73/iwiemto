﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using IWiemTo.Data;
using IWiemTo.Models;


namespace IWiemTo.Areas
{
    [Authorize]
    [Area("Biologia")] 
   
    public class QuizController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private DbBiolContext _appDbContext;             
        private Models.QuizResultModel _quizResultModel;
        private IQuizNew _quizRepo;


        public QuizController(DbBiolContext context, UserManager<ApplicationUser> userManager, IQuizNew quizNew)
        {
            _quizRepo = quizNew;
            _appDbContext = context;
            _userManager  = userManager;            
        }

        [HttpGet]
        public async Task<long> GetCurrentUserId()
        {
            ApplicationUser usr = await GetCurrentUserAsync();                        
           return usr.Id;
        }

        [HttpGet]
        public async Task<string> GetCurrentUserName()
        {
            ApplicationUser usr = await GetCurrentUserAsync();
            return usr.UserName;
        }

        private Task<ApplicationUser> GetCurrentUserAsync() => _userManager.GetUserAsync(HttpContext.User);
        
       
        public IActionResult Index()
        {
            ViewData["QuizModeList" ] = _quizRepo.Load_DictQuizMode(_appDbContext );
            ViewData["QuestionsMode"] = _quizRepo.Load_DictQuestionType( _appDbContext );
            ViewData["Subjects"     ] = _quizRepo.Load_DictCategoryView( _appDbContext );
            ViewData["Level"]         = _quizRepo.Load_DictLavelKnowledge(_appDbContext);
           // ViewBag.UserID   = 
           ViewBag.UserName = (string)GetCurrentUserName().Result;
            ViewBag.AreaName = typeof(QuizController).GetCustomAttribute<AreaAttribute>().RouteValue;
            return View();
        }
       

        public Models.QuizResultModel SetQuizResultData(int? userID)
        {
            if ( userID != null )
            {
                _quizResultModel = new Models.QuizResultModel();
                _quizResultModel.QuizCount = _appDbContext.Quiz.Where(c => c.UserID == userID).Count();
                _quizResultModel.QuizPositiveCount = _appDbContext.Quiz.Where(c => c.UserID == userID && (c.QuizResult ==1 && c.QuizStatus == 1)).Count();
                _quizResultModel.QuizNegativeCount = _appDbContext.Quiz.Where(c => c.UserID == userID && (c.QuizResult == 0  && c.QuizStatus == 1)).Count();

                return _quizResultModel;
            }
            else
                return _quizResultModel = null;
        }

     
        [HttpPost]
        public ActionResult NewQuiz(Models.Quiz quiz)
        {
            quiz.UserID = (int)GetCurrentUserId().Result;
            quiz.UserName = (string)GetCurrentUserName().Result;

            if (string.IsNullOrEmpty(quiz.QuizDescribe))
            {
                quiz.QuizDescribe = string.Format("quiz_z_{0:ddMMyyyy}_{1:hh:mm}", quiz.QuizCreateDate, quiz.QuizCreateDate);
            }

            if (ModelState.IsValid)
            {
               
                _appDbContext.Quiz.Add(quiz);
                _appDbContext.SaveChanges(); 
                
                return RedirectToAction("ShowQuestions", "Questions", quiz );
            }
           
            return View("Index", quiz);
        }


    }
}