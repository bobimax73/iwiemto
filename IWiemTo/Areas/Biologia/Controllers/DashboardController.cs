﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using IWiemTo.Data;
using IWiemTo.Models;
using Microsoft.AspNetCore.Http;



namespace IWiemTo.Areas
{

    [Authorize]
    [Area("Biologia")]

    public class DashboardController : Controller
    {
       

        private DbBiolContext _appDBContext;
        private int _userID;
        private QuizResults _quizResultRepo;
        private UserManager<ApplicationUser> _userManager;
        
        private IDashboard _dashboard;
        private Task<ApplicationUser> GetCurrentUserAsync() => _userManager.GetUserAsync(HttpContext.User);

        public DashboardController(DbBiolContext context, UserManager<ApplicationUser> userManager, IDashboard report)
        {            
            _appDBContext = context;
            _dashboard = report;             
            _userManager = userManager;
            _quizResultRepo = new QuizResults(context);
            
        }


        [HttpGet]
        public async Task<long> GetCurrentUserId()
        {
            ApplicationUser usr = await GetCurrentUserAsync();
            return usr.Id;
        }

        [HttpGet]
        public async Task<string> GetCurrentUserName()
        {
            ApplicationUser usr = await GetCurrentUserAsync();
            return usr.UserName;
        }

      

        public IActionResult Index()
        {      
            _userID = (int)GetCurrentUserId().Result;
            ViewBag.AreaName              = typeof(DashboardController).GetCustomAttribute<AreaAttribute>().RouteValue;
            ViewBag.UserID                = _userID;
            ViewBag.CountUserQuiz         = _dashboard.GetCountAllQuizByUser( _appDBContext, _userID );
            ViewBag.CountPosiviteUserQuiz = _dashboard.GetCountAllPositiveQuizByUser( _appDBContext, _userID );
            ViewBag.CountNegativeUserQuiz = _dashboard.GetCountAllNegativeQuizByUser(_appDBContext, _userID  );                     
            return View();
        }

        public ActionResult Top5Results()
        {
            _userID = (int)GetCurrentUserId().Result;
            var model = _dashboard.LoadQuizTop5Results( _appDBContext, _userID );                              
            return PartialView("~/Views/Dashboard/Top5Results.cshtml", model);
        }


        public ActionResult Top5Questions()
        {
            _userID = (int)GetCurrentUserId().Result;        
            var model = _dashboard.LoadQuizTop5Questions( _appDBContext, _userID );
            return PartialView("~/Views/Dashboard/Top5Questions.cshtml", model);
        }


        public ActionResult Top5SubCategories()
        {
            _userID = (int)GetCurrentUserId().Result;
            var model = _dashboard.LoadQuizTop5SubCategory(_appDBContext, _userID);
            return PartialView("~/Views/Dashboard/Top5SubCategories.cshtml", model);
        }

        public ActionResult ShowQuestion(int questionID)
        {
            _userID = (int)GetCurrentUserId().Result;
            var model = _dashboard.ShowQuestion(questionID, _appDBContext, _userID);
            return PartialView( "~/Views/Dashboard/ShowQuestion.cshtml" , model );
        }

        public JsonResult ProcentResults(int dateRange, DateTime dataFrom, DateTime dataTo)
        {
            _userID = (int)GetCurrentUserId().Result;           
            var model = _dashboard.GetTableProcentResults(_appDBContext, _userID, dateRange,  dataFrom, dataTo);         
            return Json(new { result = model });
        }

        public JsonResult Activities()
        {
            _userID = (int)GetCurrentUserId().Result;
            var model = _dashboard.LoadQuizActivites(_appDBContext, _userID);
            return Json(new { result = model });
        }

        [HttpPost]
        public IActionResult AllQuizDataTable(int userID)
        {

            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            // Skiping number of Rows count  
            var start = Request.Form["start"].FirstOrDefault();
            // Paging Length 10,20  
            var length = Request.Form["length"].FirstOrDefault();
            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int recordsTotal = 0;
            var quiz = _appDBContext.V_DashboardQuizSummaryInfo.Where(k=>k.UserID == userID).ToList();
            recordsTotal = quiz.Count();

            var data = quiz.Skip(skip).Take(pageSize).ToList();
            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
        }




        /// <summary>
        /// dodane 21.11.2019
        /// DataSource do obsługi tableComments -wyświetla listę  pytań od użytkownika
        /// Dashboard.js 
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>

        [HttpPost]
        public IActionResult ShowAllQuestionsByQuizID(int userID)
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            // Skiping number of Rows count  
            var start = Request.Form["start"].FirstOrDefault();
            // Paging Length 10,20  
            var length = Request.Form["length"].FirstOrDefault();
            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int recordsTotal = 0;
            var quiz = _dashboard.GetAllUsersComments(_appDBContext, userID);
            recordsTotal = quiz.Count();
            var data = quiz.Skip(skip).Take(pageSize).ToList();
            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });

        }

    }
}