﻿using IWiemTo.Data;
using IWiemTo.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace IWiemTo.Areas.Historia
{
    [Authorize]
    [Area("Historia")]
    public class QuestionsController : Controller
    {
    
        private DbHistContext _appDBContext;
        private QuizResults _quizResultRepo;
        private UserManager<ApplicationUser> _userManager;

     
        private IQuestions _questRepo;

        public QuestionsController(DbHistContext context, UserManager<ApplicationUser> userManager, IQuestions questRepo)
        {
            _appDBContext = context;
            _questRepo = questRepo;           
            _userManager = userManager;
            _quizResultRepo = new QuizResults(context);

        }
        /// <summary>
        /// Lista pytań zablkowanych - niedostępnych w puli pytań
        /// </summary>
        /// <returns></returns>
        public List<int> BlockingQuestion(int userID)
        {
            IQueryable<Models.QuestionsBlocking> questionsBlocking = null;

            List<int> blockQuestList = new List<int>();
            questionsBlocking = _appDBContext.QuestionsBlocking.Where(k=>k.UserID==userID).AsNoTracking().AsQueryable().OrderBy(n => n.QuestionID);

            foreach (var item in questionsBlocking)
            {
                blockQuestList.Add(item.QuestionID);
            }
            return blockQuestList;
        }

        [HttpGet]
        public ActionResult ShowQuestions(Models.Quiz quizData)
        {

            
            List<int> idList = null;
           // idList = new List<int> { 90, 91, 92, 93 };
            
            if (!string.IsNullOrEmpty(quizData.QuestionNumbers))
            {
                idList = _appDBContext.Questions.Where(k => k.QuestionShortDescribe.Contains(quizData.QuestionNumbers)).Select(k => k.QuestionID).ToList();
                if (idList.Count() == 0)
                {
                    idList = quizData.QuestionNumbers.Split(',').Select(s =>
                        {
                            int i;
                            return Int32.TryParse(s, out i) ? i : -1;
                        }).ToList();
                }
            }
            else
            {
                idList = BlockingQuestion(quizData.UserID);
            }
            _questRepo.Load_ChooseQuestionModeImagesFromDB(quizData, _appDBContext);
            _questRepo.Load_ChooseQuizModeImagesFromDB(quizData, _appDBContext);
            _questRepo.Load_ChooseCategoriesImagesFromDB(quizData, _appDBContext);

            IQueryable<Models.Questions> questions = null;
            questions = _appDBContext.Questions.Where(k=>k.QuestionLevelKnowledgeID==quizData.LevelKnowledgeID).
                Where(k => !string.IsNullOrEmpty(quizData.QuestionNumbers) ? idList.Contains(k.QuestionID) : !idList.Contains(k.QuestionID)).
                Where(z => quizData.QuestionsModeID.Contains(z.QuestionTypeID)).
                Select(q => new Models.Questions
            {
                QuestionsImageRel = q.QuestionsImageRel.Select(c => new Models.QuestionsImage { ImageID = c.ImageID, Image = c.Image }).ToList(),
                QuestionCategory = q.QuestionCategory.Select(c => new Models.QuestionCategory { CategoryID = c.CategoryID, CategorySubID = c.CategorySubID }).ToList(),
                QuziName = quizData.QuizName,
                QuestionID = q.QuestionID,
                QuestionText = q.QuestionText,
                QuestionPoints = q.QuestionPoints,
                QuestionsChoiceRel = (q.QuestionTypeID == 4) || (q.QuestionTypeID == 5) ?
                 q.QuestionsChoiceRel.Select(c => new Models.QuestionsChoice { QuestionChoiceID = c.QuestionChoiceID, ChoiceText = c.ChoiceText }).OrderBy(c=>c.QuestionChoiceID).ToList() :
                 q.QuestionsChoiceRel.Select(c => new Models.QuestionsChoice { QuestionChoiceID = c.QuestionChoiceID, ChoiceText = c.ChoiceText }).OrderBy(c => new Random().Next()).ToList(),
                QuestionTypeID = q.QuestionTypeID
            }
            ).AsQueryable();

            if (quizData.SubCategoryID != null)
            {
                questions = questions.Where(k => quizData.SubCategoryID.Contains(k.QuestionCategory.FirstOrDefault().CategorySubID)).OrderBy(q => new Random().Next()).Take(quizData.QuizQuestionCount);
            }
            else
            {
                questions = questions.OrderBy(q => new Random().Next()).Take(quizData.QuizQuestionCount);
            }


            ViewData["QuizName"] = quizData.QuizName;
            ViewData["QuizID"] = quizData.QuizID;
            ViewData["UserID"] = quizData.UserID;
            ViewData["DateCreate"] = quizData.QuizCreateDate;

                     
                       
            Models.QuizResult quizResult = new Models.QuizResult()
            {
                
                UserID            = quizData.QuizID,
                QuizCount         = _quizResultRepo.GetCountAllQuizByUser( quizData.UserID ),
                QuizCountPositive = _quizResultRepo.GetCountPositiveQuizByUser(quizData.UserID )

            };
            
            ViewBag.QuizData   = quizData;
            ViewBag.QuizResult = quizResult;
            ViewBag.LastQuizes = _questRepo.GetLastThreeQuizes(quizData, _appDBContext);
            ViewBag.AreaName   = typeof(QuizController).GetCustomAttribute<AreaAttribute>().RouteValue;

            return View(questions);
        }

        [HttpPost]
        public JsonResult GetAnswers(List<Models.QuizAnswers> resultQuiz)
        {
            
            List<Models.QuizAnswers> finalResultQuiz = new List<Models.QuizAnswers>();
            foreach (var item in resultQuiz)
            {

                List<Models.Answers> answersFromDB = new List<Models.Answers>();
                bool isCorectFullQuest = true;
                Models.QuizAnswers quizAnswers = new Models.QuizAnswers
                {
                    ElementID = item.ElementID,
                    IsCorect = false,
                    Answer = item.Answer,
                    QuestionID = item.QuestionID.ToString(),
                    QuestionPoints = item.QuestionPoints,
                    QuestionScore = item.QuestionScore,
                    IsCorectQuest = new List<Models.IsCorectChoice>(),
                    QuizID = item.QuizID,
                    UserID = item.UserID,
                    QuizDate = Convert.ToDateTime(item.QuizDate)
                };

                answersFromDB = _appDBContext.Answers.Where(c => c.QuestionID == Int32.Parse(item.QuestionID)).ToList();

                if ( (item.QuestionTypeID ==1 ) || (item.QuestionTypeID==2) )  // obsługa algorytmu dla pytania -> jedno lub wielokrotnego wyboru 
                {
                    isCorectFullQuest = true;

                    if (item.Answer != null)
                    {
                        if (answersFromDB.Count == item.Answer.Count)
                        {
                            foreach (var answer in answersFromDB)
                            {
                                if (!item.Answer.Contains(answer.Answer))
                                {
                                    //brak poprawnej odpowiedzi w kluczu 
                                    quizAnswers.IsCorectQuest.Add(new Models.IsCorectChoice() { IsCorect = false });
                                    quizAnswers.QuestionScore = 0;   // przyznane 0 pkt za brak poprawnej odpowiedzi
                                    isCorectFullQuest = false;
                                }
                                else
                                {
                                    quizAnswers.IsCorectQuest.Add(new Models.IsCorectChoice() { IsCorect = true });
                                    quizAnswers.QuestionScore = 1; // przyznanie 1 pkt za poprawną odpwiedz
                                }

                            }
                        }
                        else
                        {
                            foreach (var answer in answersFromDB)
                            {
                                quizAnswers.IsCorectQuest.Add(new Models.IsCorectChoice() { IsCorect = false });
                            }
                            quizAnswers.QuestionScore = 0;   // przyznane 0 pkt za brak poprawnej odpowiedzi
                            isCorectFullQuest = false;
                        }
                    }
                    else
                    {
                        foreach (var answer in answersFromDB)
                        {
                            quizAnswers.IsCorectQuest.Add(new Models.IsCorectChoice() { IsCorect = false });
                        }
                        quizAnswers.QuestionScore = 0;   // przyznane 0 pkt za brak poprawnej odpowiedzi
                        isCorectFullQuest = false;
                    }
                }

                if ((item.QuestionTypeID == 3) || (item.QuestionTypeID == 4) || (item.QuestionTypeID == 5)) // pytanie otwarte
                {
                    if (answersFromDB.Count == item.Answer.Count)  // sparwdzenie czy liczba odpowiedzi z bazy i usera jest tak sama
                    {
                        isCorectFullQuest = true;
                        var poz = 0;
                        foreach (var id in item.QuestionChoiceID)
                        {
                            foreach (var odpZbazy in answersFromDB.Where(c => c.QuestionChoiceID == id))
                            {
                                if (item.Answer[poz] == null) item.Answer[poz] = "";
                                    double rank = 0.00;
                                    bool isCorect = false;
                                    using (Npgsql.NpgsqlCommand npgSqlCommand = new Npgsql.NpgsqlCommand())
                                    {
                                        string connectString = _appDBContext.Database.GetDbConnection().ConnectionString;
                                        Npgsql.NpgsqlConnection connect = new Npgsql.NpgsqlConnection(connectString);
                                        if (connect.State == System.Data.ConnectionState.Closed) connect.Open();
                                        npgSqlCommand.Connection = connect;
                                        npgSqlCommand.CommandText = "check_answer_key";
                                        npgSqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                                        npgSqlCommand.Parameters.Add(new Npgsql.NpgsqlParameter("answeruser", NpgsqlTypes.NpgsqlDbType.Text)).Value = item.Answer[poz];
                                        npgSqlCommand.Parameters.Add(new Npgsql.NpgsqlParameter("answerkey", NpgsqlTypes.NpgsqlDbType.Text)).Value = odpZbazy.SearchVector;
                                        Npgsql.NpgsqlDataReader dr = npgSqlCommand.ExecuteReader();
                                    
                                       while (dr.Read())
                                        {
                                            rank = (double)dr[0];
                                            isCorect = (bool)dr[1];
                                            dr.NextResult();
                                        }
                                    connect.Close();
                                    }
                                if (!isCorect)
                                {
                                    quizAnswers.IsCorectQuest.Add(new Models.IsCorectChoice() { IsCorect = false });
                                    isCorectFullQuest = false;
                                }
                                else
                                {
                                    quizAnswers.IsCorectQuest.Add(new Models.IsCorectChoice() { IsCorect = true });
                                    quizAnswers.QuestionScore++;  // przyznanie 1 pkt za poprawną odpwiedz
                                };
                                
                                //    break;                                
                            }
                            poz++;
                        }
                    }
                    else
                    {
                        isCorectFullQuest = false;
                    }
                }



                quizAnswers.IsCorectJson = JsonConvert.SerializeObject(quizAnswers.IsCorectQuest);               
                quizAnswers.IsCorect = isCorectFullQuest;
                item.QuestionScore = item.QuestionScore + quizAnswers.QuestionScore;
                finalResultQuiz.Add(quizAnswers);

                AddQuestionsToQuiz(quizAnswers);
             
               

            }
            // tutaj  update tabeli quiz
            //  pola QuizScore, QuizStatus            



            UpdateCurrentQuiz(resultQuiz);
            _appDBContext.SaveChanges();
            return Json(finalResultQuiz);
        }

        /// <summary>
        /// dodanie rekordu - pojdyńczego pytania  do tabeli QuizDetailQuestions
        /// </summary>
        /// <param name="quizAnswer"></param>

        public void AddQuestionsToQuiz( Models.QuizAnswers quizAnswer)
        {
            int QuestionID = 0;
            Int32.TryParse(quizAnswer.QuestionID, out QuestionID);

            Models.QuizQuestionsDetail quizDetail = new Models.QuizQuestionsDetail()
            {
                QuizID      = quizAnswer.QuizID,
                UserID      = quizAnswer.UserID,
                QuestionID  = QuestionID,
                DateCreate  = quizAnswer.QuizDate,
                IsCorect    = quizAnswer.IsCorect,
                Score       = quizAnswer.QuestionScore
            };
            _appDBContext.QuizQuestionsDetail.Add(quizDetail);
        }



        public void UpdateCurrentQuiz(List<Models.QuizAnswers> quizAnswers)
        {
            int? quizID       = quizAnswers.Select(c => c.QuizID).FirstOrDefault();
            var countQuest    = quizAnswers.Count();                           // liczba pytań w Quizie
            var scoreQuiz     = quizAnswers.Sum(c => c.QuestionPoints);                     // całkowita liczba punktów Quizu
            var scorePerQuest = quizAnswers.Sum(c => c.QuestionScore);                     // liczba punktów za pytanie uzyskana przez usera
            var scoreUser = Math.Round((decimal)((scorePerQuest) * 100) / scoreQuiz, 2);  //  procent uzyskany na quizie 

            if (quizID != null)
            {
                Models.Quiz quizModel = _appDBContext.Quiz.Where(c => c.QuizID == quizID).FirstOrDefault();
                quizModel.QuizQuestionCount = countQuest;
                quizModel.QuizStatus = 1;
                quizModel.QuizResult = (scoreUser >= 60)  ? 1 : 0 ;            // 1 quiz zaliczony 0 - quiz niezaliczony
                _appDBContext.Entry(quizModel).State = EntityState.Modified;
            }

        }


        [HttpPost]
        public JsonResult GetAllAnswers( Models.AllAsnwers question)
        {
            Models.AllAsnwers answers = _appDBContext.Answers.Where(c => c.QuestionID == question.QuestionID).Select(
                c => new Models.AllAsnwers
                { QuestionID = question.QuestionID,
                  QuestionText = question.QuestionText,
                  AnswerText   = c.Answer
                }).FirstOrDefault();
            return Json(answers);
        }


        [HttpPost]
        public JsonResult GetAllAnswersNew(Models.AllAsnwers question)
        {

            //List<Models.AllAsnwers> finalResultQuiz = new List<Models..AllAsnwers>();
            var z = 0;
            List<Models.AllAsnwers> answers = new List<Models.AllAsnwers>();
            List<Models.Answers> answersFromDB = new List<Models.Answers>();
            answersFromDB = _appDBContext.Answers.Where(c => c.QuestionID == question.QuestionID).OrderBy(c =>c.QuestionChoiceID).ToList();
            foreach(var item in answersFromDB)
            {
                answers.Add(
                    new Models.AllAsnwers
                    {
                        ElementID = question.ElementID,
                        QuestionID = question.QuestionID,
                        QuestionText = question.QuestionText,
                        AnswerText = item.Answer,
                        IsCorect = question.IsCorectSubQuest[z].IsCorect
                    }
                    );
                z++;
            }
           
            return Json(answers);
        }

        public IActionResult Index()
        {
            return View();
        }


        [HttpGet]
        public ActionResult ShowDetailResult(int scoreUserSum, int scoreQuizSum, int scoreProcent, int countCorectAns, int countNotCorectAns)
        {
            ViewBag.scoreUserSum = scoreUserSum;
            ViewBag.scoreQuizSum = scoreQuizSum;
            ViewBag.scoreProcent = scoreProcent;
            ViewBag.countCorectAns = countCorectAns;
            ViewBag.countNotCorectAns = countNotCorectAns;
            return PartialView("ResultDetail");
        }

        



    }
}