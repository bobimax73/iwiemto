﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using IWiemTo.Models;
using IWiemTo.Data;


namespace IWiemTo.Areas.Historia
{
    [Authorize]
    [Area("Historia")]

    public class EditController : Controller
    {

        private DbHistContext _appDBContext;
        private IQuestionsEditor _repo;
        private string AreaName; 

        public EditController(DbHistContext context, UserManager<ApplicationUser> userManager, IQuestionsEditor questEdytor)
        {
            _appDBContext = context;
            _repo = questEdytor;
            AreaName= typeof(EditorController).GetCustomAttribute<AreaAttribute>().RouteValue;

        }

        public IActionResult Index( int questionID)
        {
            Models.Questions quest = _appDBContext.Questions.Where(item => item.QuestionID == questionID).FirstOrDefault();
            _appDBContext.Entry(quest).Collection(p => p.QuestionCategory).Load();
            _appDBContext.Entry(quest).Collection(p => p.QuestionsImageRel).Load();
            ViewBag.QuestionID = questionID;
            quest.SelectedCategory = quest.QuestionCategory.FirstOrDefault().CategoryID;
            quest.SelectedSubCategory = quest.QuestionCategory.FirstOrDefault().CategorySubID;
            ViewBag.AreaName = AreaName;
            ViewBag.CategoryList = _repo.GetCategories(_appDBContext);            
            ViewBag.SubCategoryList = _repo.GetSubCategoriesByCategoryID(_appDBContext, quest.SelectedCategory);
            ViewBag.TypeQuestion = _repo.LoadTypeOfQuestionContent(_appDBContext);
            ViewBag.LevelKnowledge = _repo.GetLevelKnowledge(_appDBContext);
            return View(quest);
        }

        [HttpPost]
        public ActionResult EditQuestion(Models.Questions quest)
        {
            string previusPage = "Editor";
            Models.Questions record = _appDBContext.Questions.Where(item => item.QuestionID == quest.QuestionID).FirstOrDefault();
            _appDBContext.Entry(record).Collection(p => p.QuestionCategory).Load();
            _appDBContext.Entry(record).Collection(p => p.QuestionsImageRel).Load();
            if (ModelState.IsValid)
            {
                //podstawie pod pola edytowanego rekordu
                record.QuestionText = quest.QuestionText;                
                record.QuestionCategory.FirstOrDefault().CategoryID    = quest.SelectedCategory;
                record.QuestionCategory.FirstOrDefault().CategorySubID = quest.SelectedSubCategory;
                record.QuestionPoints = quest.QuestionPoints;
                record.QuestionSource = quest.QuestionSource;
                record.QuestionExtended = quest.QuestionExtended;
                record.QuestionShortDescribe = quest.QuestionShortDescribe;

                record.QuestionsImageRel.FirstOrDefault().Image = quest.QuestionsImageRel.FirstOrDefault().Image;
                record.QuestionLevelKnowledgeID = quest.QuestionLevelKnowledgeID;
                _appDBContext.SaveChanges();
                
                return RedirectToAction("Index", previusPage);
            }
          
            return RedirectToAction("Index", previusPage);
        }

        public JsonResult GetSubCategoryByCategoryId(string categoryID)
        {
            if (String.IsNullOrEmpty(categoryID))
            {
                throw new ArgumentNullException("categoryId");
            }
            int id = Int32.Parse(categoryID);
            var subCategoryList = _repo.GetSubCategoriesByCategoryID(_appDBContext, id);
            return Json( subCategoryList );
        }

    }
}