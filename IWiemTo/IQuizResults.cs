﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IWiemTo.Models;


namespace IWiemTo
{
    public interface IQuizResults  : IDisposable
    {
        void QuizResultLoadData(int? userID);

        int GetCountAllQuizByUser(int? userID );

        int GetCountPositiveQuizByUser(int? userID);
    }
}
