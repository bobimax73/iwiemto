﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IWiemTo.Models
{
    public class QuizResult : Quiz
    {
        public string QuizPositiveImage = "trophy.png";

        public int QuizCount { get; set; }

        public int QuizCountPositive { get; set; }

        public int QuizCountNegative { get; set; }

    }
}
