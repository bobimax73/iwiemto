﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IWiemTo.Models
{
    public class TTickets
    {
        [Key]
        public int QuizQuestionsDetailCommentsID { get; set; }

        public int Status { get; set; }

        public int CommentsTypeID { get; set; }

        public int QuizQuestionDetailID { get; set; }

        public DateTime DateCreate { get; set; }

        public DateTime DateUpdate { get; set; }

        public string Comments { get; set; }
        
        [NotMapped]
        public string CommentsType { get; set; }


    }

    


}
