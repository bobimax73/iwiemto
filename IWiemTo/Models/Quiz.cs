﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IWiemTo.Models
{
    public class Quiz
    {
        [Key]
        public int QuizID { get; set; }

        [Required]
        public string QuizName { get; set; }

        public DateTime QuizCreateDate { get; set; }

        public int QuizResult { get; set; }

        public int QuizStatus { get; set; }

        public int QuizQuestionCount { get; set; }

      //  [Required(ErrorMessage = "Wprowadź temat quizu lub jego etykietę")]
        public string QuizDescribe { get; set; }

    
        [ForeignKey("Users")]
        public int UserID { get; set; }



        [NotMapped]
        public string UserName { get; set; }


        [NotMapped]
        public string QuizModeName { get; set; }

      
        
        [NotMapped]
        public int[] QuestionsModeID { get; set; }


       // [NotMapped]
       // public List<string> QuestionsModeImage { get; set; }

       // [NotMapped]
       // public List<string> QuestionsModeName{ get; set; }

        [NotMapped]
        public List<ModelQuestionsModeData> QuestionsModeData { get; set; }

        [ForeignKey("QuizMode")]
        public int QuizModeID { get; set; }

        [NotMapped]
        public List<string> QuizModeImage { get; set; }


        [NotMapped]
        public int[] SubCategoryID { get; set; }


     
        
        [NotMapped]
        public List<ModelSubCategoryData> SubCategoryData { get; set; }


        [NotMapped]        
        public QuizResultModel ResultData { get; set; }


        public int LevelKnowledgeID { get; set; }

        [NotMapped]
        public string LevelKnowledgeName { get; set; }

        // Numery pytań podanych przez ucznia
        [NotMapped]
        public string QuestionNumbers { get; set; }

    }

    /// <summary>
    /// Klasa definuje wniki quizu
    /// </summary>
    public class QuizResultModel
    {
       // public const string QuizPositiveImage = "trophy.png";

        public int QuizCount { get; set; }
        
        public int QuizPositiveCount { get; set; }

        public int QuizNegativeCount { get; set; }
       
    }


    public class ModelQuestionsModeData
    {
        public string Data1 { get; set; }
        public string Data2 { get; set; }
    }

    public class ModelSubCategoryData
    {
        public string Data1 { get; set; }
        public string Data2 { get; set; }
    }

    public class Dict_QuizMode
    {
        [Key]
        public int QuizModeID { get; set; }

        public string QuizModeName { get; set; }

        public string QuizModeImage { get; set; }
    }

    public class QuizQuestionsDetail
    {
        [Key]
        public int QuizQuestionDetailID { get; set; }

        [ForeignKey("Quiz")]
        public int QuizID { get; set; }

        public int UserID { get; set; }

        public int QuestionID { get; set; }


        public DateTime DateCreate { get; set; }


        public bool IsCorect { get; set; }

        public int Score { get; set; }

        public virtual QuizQuestionsDetailComments QuizQuestionsDetailComments { get; set; }
    }


    public class QuestionsBlocking
    {
        [Key]
        public int QuizQuestionDetailID { get; set; }

        [ForeignKey("Quiz")]
        public int QuizID { get; set; }

        public int UserID { get; set; }

        public int QuestionID { get; set; }

        public bool IsCorect { get; set; }
    }

    public class Dict_CategoryQuestionView
    {
        public int CategoryID { get; set; }

        public int SubCategoryID { get; set; }

        public string CategoryName { get; set; }

        public string SubCategoryName { get; set; }
    }

    // add 14.09.2019
    // klasa mapuje tabele z bazy danych QuizQuestionsDetailComments
    
    public class QuizQuestionsDetailComments
    {
        [Required]
        public int QuizQuestionsDetailCommentsID { get; set; }

        [Required]
        [ForeignKey("QuizQuestionsDetail")]
        public int QuizQuestionDetailID { get; set; }

        
        [Required(ErrorMessage = "Komentarz jest wymagany")]        
        public string Comments { get; set; }

        [Required]
        [ForeignKey("Dict_CommentsType")]
        public int CommentsTypeID { get; set; }

        public virtual QuizQuestionsDetail QuizQuestionsDetail { get; set; }
    }
}
