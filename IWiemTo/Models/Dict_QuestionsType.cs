﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IWiemTo.Models
{
    public class Dict_QuestionsType
    {
        public int TypeID { get; set; }
        public string TypeName { get; set; }
        public string TypeNameImage { get; set; }
    }

}
