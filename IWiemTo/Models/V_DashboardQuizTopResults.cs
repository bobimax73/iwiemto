﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IWiemTo.Models
{
    public class V_DashboardQuizTopResults 
    {

        public string   QuizName { get; set; }

        public string   QuizModeName { get; set; }

        public DateTime QuizCreateDate { get; set; }  

        public int      QuizQuestionCount { get; set; }

        public int      UserScore { get; set; }

        public int      QuizAllScore { get; set;  }

        public int      Procent { get; set; }

        public int      UserID { get; set; }

        public int      QuizID { get; set; }

        public string   QuizDescribe { get; set; }                 // opis quizu notatka 

        public int      QuestionsCountPositive { get; set; }       // liczba pytań z odpowiedzia pozytywna

        public int      QuestionsCountNegative { get; set;  }      // liczba pytań z odpowiedzia negatywna
    }
}
