﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IWiemTo.Models
{

    /// <summary>
    /// Klasa mapująca widok V_DashboardQuizProcentResults
    /// Widok zwraca wyniki procentowe globalnie wdla wszystkich quizow.
    /// </summary>
    public class V_DashboardQuizProcentResults
    {
       public int QuizID  { get; set; }

       public int UserID  { get; set; }

       public int Procent { get; set; }

       public DateTime QuizData { get; set; }
    }

    /// <summary>
    /// klasa reprezentuje obiekt
    /// przechowujacy zakres procentowy i całkowita liczbe wyników quizów które są w tym zakresie
    /// </summary>
    public class ProcentResultsRange
    {
        
        public string RangeName { get; set; }

        public int ProcentResultCount { get; set; }

    }
}
