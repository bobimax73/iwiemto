﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IWiemTo.Models
{
    public class V_DashboardQuizActivites
    {
        public int UserID { get; set; }

        public int QuizCount { get; set; }

        [DataType(DataType.Date)]
        public DateTime QuizCreateDate { get; set;  }
    }
}
