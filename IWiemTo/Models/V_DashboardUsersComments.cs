﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IWiemTo.Models
{
    /// <summary>
    /// relacja mapuje obiekt z bazy : VIEW  V_DashboardUsersComments
    /// dodane: 21.11.2019
    /// </summary>
   
    public class V_DashboardUsersComments
    {
        public int QuizQuestionsDetailCommentsID { get; set; }

        public int   QuizID   { get; set; }

        public int UserID { get; set; }

        public string Comments { get; set; }

        public string  QuizName { get; set; }

        [System.ComponentModel.DataAnnotations.DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{yyyy-MM-dd HH:mm}")]
        public DateTime DateCreate { get; set; }

        public DateTime? DateUpdate { get; set; }

        public int Status { get; set;  }

        public string CommentsType { get; set;  }
    }
}
