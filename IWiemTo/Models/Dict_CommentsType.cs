﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IWiemTo.Models
{
    public class Dict_CommentsType
    {
        public int    CommentsTypeID { get; set; }
        public string CommentsType   { get; set; }
    }
}
