﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace IWiemTo.Models
{
    public class Questions
    {
        public int    QuestionID { get; set; }
        public string QuestionText { get; set; }
        public string QuestionTextasImage { get; set; }
        public int    QuestionTypeID { get; set; }
        public int    QuestionPoints { get; set; }                              // punktacja za pytanie
        public string QuestionSource { get; set; }                              // zrodło skąd pochodzi pytanie

        public int  QuestionExtended { get; set; }                              // pole okresla rodzaj tresci 0:podstawowe 1: rozszerzone

        public List<QuestionsChoice> QuestionsChoiceRel { get; set; }
        public virtual ICollection<QuestionsImage>  QuestionsImageRel { get; set; }

        [NotMapped]
        [Required(ErrorMessage = "Proszę wybrać kategorię")]
        public int SelectedCategory { get; set; }

        [NotMapped]
        public int SelectedSubCategory { get; set; }

        [NotMapped]
        public string QuziName { get; set; }

        [NotMapped]
        public int QuziID { get; set; }

        [NotMapped]
        public int UserID { get; set; }

     
        public DateTime QuestionCreated { get; set; }

        public ICollection<QuestionCategory> QuestionCategory { get; set; }     
        
        public int QuestionLevelKnowledgeID { get; set; }

        public string QuestionShortDescribe { get; set; }          // krótki opis czego dotyczy pytanie
    }

    public class QuestionsView
    {
        public int QuestionID { get; set; }
        public string QuestionText { get; set; }
        public string QuestionTextasImage { get; set; }
        public int QuestionTypeID { get; set; }
        public int QuestionPoints { get; set; }                              // punktacja za pytanie
        public string QuestionSource { get; set; }                              // zrodło skąd pochodzi pytanie
        public string TypeName { get; set; }
        public  int CategoryID { get; set; }
        public int CategorySubID { get; set; }
        public string CategoryName { get; set; }
        public string SubCategoryName { get; set; }

        public string QuestionTypeContent { get; set; }

        [ForeignKey("Dict_LevelKnwoledge")]
        public int QuestionLevelKnowledgeID { get; set; }

        public string LevelKnowledgeName { get; set; }

        public string QuestionShortDescribe { get; set; }
    }






    public class QuestionsChoice
    {
        [Key]
        public int QuestionChoiceID { get; set; }
        public int QuestionID { get; set; }
        public string ChoiceText { get; set; }
        public bool IsCorect { get; set; }

        public Questions Question { get; set; }

        public V_DashboardQuizQuestions V_DashboardQuizQuestions { get; set; }
    }

    
    public class QuestionsImage
    {
        [Key]
        public int ImageID { get; set; }
      
        public int QuestionID { get; set; }

        public string Image { get; set; }

        public Questions Quest { get; set; }

        public V_DashboardQuizQuestions V_DashboardQuizQuestions { get; set; }
    }

    
    public class QuestionCategory
    {
        [Key]
        public int QuestionCategoryID { get; set; }
        [ForeignKey("Questions")]
        public int QuestionID { get; set; }
        public int CategoryID { get; set; }
        public int CategorySubID { get; set; }
    }

    /// <summary>
    /// model reprezentujacy informacje ogolne o bazie pytań
    /// </summary>
    public class QuestionsGeneralInfo
    { 

        //całkowita ilość pytań
        public int AllCount { get; set; }

        //całkowita ilość pytań rozszerzonych
        public int ExtendedCount { get; set; }

        //całkowita ilość pytań podstawowych
        public int BasicCount { get; set; }

       //data i czas ostatniej aktualizacji 
       public DateTime  UpdateDate { get; set; }

    }
}

