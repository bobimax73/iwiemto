﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;


namespace IWiemTo.Models
{
   public class Dict_Category
    {
        
         public int CategoryID { get; set; }
         public string CategoryName { get; set; }
         public List<Dict_SubCategory> SubCategory { get; set; }
    }

    public class Dict_SubCategory
    {
        public int SubCategoryID { get; set; }
        [ForeignKey("Dict_Category")]
        public int CategoryID { get; set; }
        public string SubCategoryName { get; set; }

        public string SubCategoryImage { get; set; }
        
    }

    public class Dict_CategoryView
    {
        public IEnumerable<SelectListItem> Category    { get; set; }
        public IEnumerable<SelectListItem> SubCategory { get; set; }

    }

    public class MyViewModel
    {
        public int[] SelectedItemIds { get; set; }
        public MultiSelectList Items { get; set; }
    }


    /// <summary>
    /// słownik grup wyników procentowe zakresy
    /// </summary>
    public class Dict_ResultsRange
    {
        [Key]
        public int ResultsRangeID  { get; set;  }

        public string RangeName    { get; set;  }

        public int ResultMinValue { get; set;  }

        public int ResultMaxValue { get; set;  } 

    }

    /// <summary>
    /// Model dla relacji  w bazie -  Dict_LevelKnowledge - słownik pzoiom wiedzy :2. szkoła średnia/1. szkoła podstawowa
    /// </summary>
    /// 
    [Table("Dict_LevelKnowledge")]
    public class LevelKnowledgeTable
    {
        [Key]
        public int    LevelKnowledgeID   { get; set; }

        [Required]
        public string LevelKnowledgeName { get; set; }
    }


}
