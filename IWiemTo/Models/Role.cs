﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IWiemTo.Models
{
    public class Role
    {
        public int UserID      { get; set; }

        public string RoleName { get; set; }

        public virtual Models.User Usersl { get; set; }
    }
}
