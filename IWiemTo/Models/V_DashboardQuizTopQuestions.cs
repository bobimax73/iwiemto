﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IWiemTo.Models
{
    public class V_DashboardQuizTopQuestions
    {
        public int    QuestionID    { get; set; }

        public int    QuestionCount { get; set; }

        public string QuestionText  { get; set; }

        public int    UserID        { get; set; }

        public string TypeName      { get; set; }
    }
}
