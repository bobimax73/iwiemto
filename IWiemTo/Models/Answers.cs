﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
//using Npgsql.EntityFrameworkCore.PostgreSQL;

namespace IWiemTo.Models
{
    public class Answers
    {
        
        public int AnswerID { get; set; }
        [ForeignKey("Questions")]
        public int QuestionID { get; set; }
        public int QuestionChoiceID { get; set; }
        public string Answer { get; set; }
        public string SearchVector { get; set; }

       // public NpgsqlTypes.NpgsqlTsVector SearchVectorN { get; set; }
    }
    /// <summary>
    /// kla uzywana jako model odpowiedzi dla quizu
    /// </summary>
    public class QuizAnswers
    {
        public string QuestionID { get; set; }                    // id pytania
        public string QuestionText { get; set; }                 // treść pytania
        public int    QuestionPoints { get; set; }                // punktacja za pytanie
        public int    QuestionScore  { get; set; }                // punkty uzyskane za odpowiedz
        public List<string> Answer { get; set; }                // odpowiedzi uzytkownika      
        public bool IsCorect { get; set; }                      // wskazuje czy całkowita odpowiedź jest poprawna : true wtedy gdy wszystkie pododpwoiedzi sa prawdziwe
        public int ElementID { get; set; }      
        public int QuestionTypeID { get; set; }
        public List<int> QuestionChoiceID { get; set; }               
        public List<IsCorectChoice> IsCorectQuest { get; set; }
        public string IsCorectJson { get; set; }              // pomocnicza zmienna do utworzenia tablicy w Json i przekazania z html

        public int QuizID { get; set; }
        public int UserID { get; set; }

        public DateTime QuizDate { get; set; }
    }

    public class IsCorectChoice
    {
        public bool IsCorect { get; set; } 
    }

   
    
    public class AllAsnwers
    {
        public int QuestionID { get; set; }
        public string QuestionText{ get;set;}
        public string AnswerText { get; set; }
        public bool IsCorect { get; set; }
        public int ElementID  { get; set; }
        public List<IsCorectChoice> IsCorectSubQuest { get; set; }
    }

    // Element Html na stronie odpowiadajacy pytaniu i odpowiedziom do tego pytania
    public class WebElementProperty   
    {
         public int RootId { get; set; }
         public int ChildId { get; set; }
    }


}
