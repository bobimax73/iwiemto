﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IWiemTo.Models
{
    public class V_DashboardQuizTopSubCategory
    {
        public int    UserID { get; set; }

        public string SubCategoryName { get; set; }

        public int    QuestionCount { get; set; }

        public string SubCategoryImage { get; set;  }
    }
}
