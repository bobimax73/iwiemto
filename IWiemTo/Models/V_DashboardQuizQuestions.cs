﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IWiemTo.Models
{
    public class V_DashboardQuizQuestions
    {
        public int    QuestionID    { get; set; }

        public int    QuizID        { get; set; }

        public int    SubCategoryID { get; set; }

        public int    QuestionPoints { get; set; }
          
        public string QuestionText  { get; set; }

        public string CategoryName  { get; set; }

        public string SubCategoryName { get; set; }
        
        public string QuestionSource { get; set; }

        public string TypeName { get; set; }

        public int    TypeID { get; set; }

        public int    SubQuestionCount { get; set; }

        public int    QuestionExtended { get; set; }
    
        public virtual ICollection<QuestionsImage> QuestionsImageRel1 { get; set; }

        public virtual ICollection<QuestionsChoice> QuestionsChooseRel { get; set; }

        public bool IsCorect { get; set; }

        public int Score { get; set; }
    }

    
}
